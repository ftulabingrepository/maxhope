<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class encashmenttype extends Model
{
    protected $table = 'encashment_types'; 
    protected $fillable = [
        'name'
    ];
}
