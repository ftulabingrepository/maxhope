<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiergroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiergroups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('groupid');
            $table->string('groupname');
            $table->integer('tierid');
            $table->integer('parentid');
            $table->integer('ancestor');
            $table->integer('left');
            $table->integer('right');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiergroups');
    }
}
