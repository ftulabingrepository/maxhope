<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class revenue_type extends Model
{
    protected $table = 'revenue_types'; 
    protected $fillable = [
        'name', 'percent'
    ];
}
