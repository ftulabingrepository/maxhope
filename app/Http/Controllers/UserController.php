<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use DB;
use Auth;
use Carbon;
use DateTime;
use App\movements, App\earnings, App\earningTypes, App\User, App\userFunds, App\tiergroups, App\invites, App\referals;
use App\encashments;
use App\revenue;
use App\admin_fee;
use App\other_charges;
use App\tax;
use App\encashmentmethod;
use App\Notifications\NotifyAdmin;
use Pusher\Pusher;
use App\NotificationLog;
use App\activity_logs;

class UserController extends Controller
{
    private $perksRewards = [];

    public function __construct(){
        $perks = DB::table('earning_types')->get();

        foreach($perks as $perk){
            $amount = $perk->eAmount;
            array_push($this->perksRewards, $amount);
        }
    }

    public function dashboard(){

        $userid = Auth::user()->id;
        $regFunds = $this->getFunds($userid, 'registrationFunds');
        $payoutFunds = $this->getFunds($userid, 'payoutFunds');
        $accumulated = $this->getAccumulated($userid);
        if(!$this->isGenerated($userid)){
            $accumulated += 2500;
        }
        $encashmentmethods = $this->getEnchasmentMethod();
        $recipientAvailable = $this->getRecipient();
        $groupID = $this->currentGroup();
        $forTier = $this->currentGroup2();
        $myAccounts = $this->getMyAccounts($userid);

        if($groupID){
            $tier = $this->getGroupTier($forTier);
            $allGroups = $this->getAllGroups();

            $hide = ($tier > 1 ? 'none' : 'block');

            $utables = [];
            $userTables = $this->userTables($userid);
            foreach($userTables as $table){
                array_push($utables, $table->groupid);
            }
     
            $currentTable = $this->getCurrentTable($groupID);

                $movement1 = DB::table('movements as m')
                    ->select('m.groupid','m.userid','m.movedto','m.origin','u.username', 'u.firstname', 'u.lastname', 'u.mobileno')
                    ->leftJoin('tiergroups as t', 'm.groupid', '=', 't.id')
                    ->leftJoin('users as u', 'm.userid', '=', 'u.id')
                    ->where('t.tierid', 1)
                    ->where('t.id', $allGroups[1])
                    ->whereIn('m.groupid', $utables)
                    ->get();

                $movement2 = DB::table('movements as m')
                    ->select('m.groupid','m.userid','m.movedto','m.origin','u.username', 'u.firstname', 'u.lastname', 'u.mobileno')
                    ->leftJoin('tiergroups as t', 'm.groupid', '=', 't.id')
                    ->leftJoin('users as u', 'm.userid', '=', 'u.id')
                    ->where('t.tierid', 2)
                    ->where('t.id', $allGroups[2])
                    ->whereIn('m.groupid', $utables)
                    ->get();

                $movement3 = DB::table('movements as m')
                    ->select('m.groupid','m.userid','m.movedto','m.origin','u.username', 'u.firstname', 'u.lastname', 'u.mobileno')
                    ->leftJoin('tiergroups as t', 'm.groupid', '=', 't.id')
                    ->leftJoin('users as u', 'm.userid', '=', 'u.id')
                    ->where('t.tierid', 3)
                    ->where('t.id', $allGroups[3])
                    ->whereIn('m.groupid', $utables)
                    ->get();

                $movement4 = DB::table('movements as m')
                    ->select('m.groupid','m.userid','m.movedto','m.origin','u.username', 'u.firstname', 'u.lastname', 'u.mobileno')
                    ->leftJoin('tiergroups as t', 'm.groupid', '=', 't.id')
                    ->leftJoin('users as u', 'm.userid', '=', 'u.id')
                    ->where('t.tierid', 4)
                    ->where('t.id', $allGroups[4])
                    ->whereIn('m.groupid', $utables)
                    ->get();

                $movement5 = DB::table('movements as m')
                    ->select('m.groupid','m.userid','m.movedto','m.origin','u.username', 'u.firstname', 'u.lastname', 'u.mobileno')
                    ->leftJoin('tiergroups as t', 'm.groupid', '=', 't.id')
                    ->leftJoin('users as u', 'm.userid', '=', 'u.id')
                    ->where('t.tierid', 5)
                    ->where('t.id', $allGroups[5])
                    ->whereIn('m.groupid', $utables)
                    ->get();

                $movement6 = DB::table('movements as m')
                    ->select('m.groupid','m.userid','m.movedto','m.origin','u.username', 'u.firstname', 'u.lastname', 'u.mobileno')
                    ->leftJoin('tiergroups as t', 'm.groupid', '=', 't.id')
                    ->leftJoin('users as u', 'm.userid', '=', 'u.id')
                    ->where('t.tierid', 6)
                    ->where('t.id', $allGroups[6])
                    ->whereIn('m.groupid', $utables)
                    ->get();

                $movements = [0, $movement1, $movement2, $movement3, $movement4, $movement5, $movement6];
 
            return view('dashboard', compact('hide', 'movements', 'accumulated', 'regFunds', 'payoutFunds', 'currentTable', 'encashmentmethods', 'myAccounts', 'recipientAvailable'));

        }else{
            $movements = false;
            $hide = 'none';
            return view('dashboard', compact('hide', 'movements', 'accumulated', 'regFunds', 'payoutFunds', 'currentTable', 'encashmentmethods', 'myAccounts', 'recipientAvailable'));
        }

    }

    public function userRegFunds(Request $request){
        $userid = Auth::user()->id;
        $regFunds = $this->getFunds($userid, 'registrationFunds');
        $findAcc = User::where('username', $request->newUsername)->count();

        if($findAcc > 0){
         return Redirect::back()->withErrors(['Username already exist.']);
        }

        if($regFunds >= 1580){   
            return $this->addAccount($request);
        }else {
            return Redirect::back()->withErrors(['Insufficient registration funds.']);
        }
    }

    private function addAccount($request){
        //check group count...  *with parent 0
        $userid = Auth::user()->id;
        $groupID = $this->currentGroup();
        $parentID = $this->getParentId($groupID);
        $tier = $this->getCurrentTable($groupID);

        if($parentID > 0){
            //group has parent   
            $groupCount = $this->getGroupCount($groupID);

            if($groupCount < 13){

                $newSlot = $groupCount + 1;

                $regFunds = $this->checkRegistrationFunds();

                if($regFunds){
                    $newUserID = $this->addNewUserReturnId($request);

                    if($newUserID){
                        $this->addToInvites($userid, $newUserID);
                        $this->addEarnings($userid, 9);
                        $this->addIndirect($userid);
                    }
                }
                
                $splitMove = $this->splitTableMovement($groupID, $newUserID, $newSlot);

                if($splitMove){

                    //check again if table is full
                    $groupCountN = $this->getGroupCount($groupID);
                    if($groupCountN >= 13){
                        $newGroups = $this->newGroups($groupID, $tier);
                    }
                }

            }else{
                //puno nang lamisa
                //$newGroups = $this->newGroups($groupID, $tier);
            }
                    
        }else{
            
            $groupCount = $this->getGroupCount($groupID);

            if($groupCount < 13){

                //if not full Add new user...
                $newUserID = $this->addNewUserReturnId($request);
                if($newUserID){
                    $this->addToInvites($userid, $newUserID);
                    $this->addEarnings($userid, 9);
                    $this->addIndirect($userid);
                }

                if($newUserID){

                    $pushMember = $this->newMovement($newUserID, $groupID);

                    //check again if table is full for split...
                    if($pushMember){
                        
                        $newGroupCount = $this->getGroupCount($groupID);

                        if($newGroupCount >= 13){
                            $newGroups = $this->newTierGroups($groupID, $tier);
                        }
                    }
                }

            }else{
                //full na ang table...
            }
                
        }

        return redirect('dashboard');
    }

    //get total members in group...
    private function getGroupCount($id){
       return $groupCount = DB::table('movements as m')
                            ->select('m.groupid','m.userid','m.movedto','m.origin','u.username')
                            ->leftJoin('users as u', 'm.userid', '=', 'u.id')
                            ->where('m.userid', '>', 0)
                            ->where('m.groupid', $id)
                            ->count();
    }

    private function getParentId($groupid){

        $parent = DB::table('tiergroups')
                        ->select('parentid')
                        ->where('id', $groupid)
                        ->first();

        return $parent->parentid;
    }

    private function cpsDistributionValue(){
        $dt = Carbon::now();
        $realUsers = DB::table('users as u')->select('id')
                        ->whereRaw('id NOT IN(SELECT userid from sys_accounts)')
                        ->where('type', 2)
                        ->get();

        $cps = DB::table('earning_types')->select('eAmount')->where('id', 11)->first();
        $fake = DB::table('sys_accounts')
                        ->count();

        $real = DB::table('users')
                        ->where('type', 2)
                        ->count();

        $diff = (int)$real - (int)$fake;

        $cdv = (float)$cps->eAmount / (int)$diff;

        DB::update('update userfunds set payoutFunds = payoutFunds +'.$cdv.' WHERE userid NOT IN (SELECT userid FROM sys_accounts)');

        $tValues = [];
        foreach ($realUsers as $user) {

            array_push($tValues, ['userid' => $user->id , 'earningTypeId' => 11, 'amount' => $cdv , 'created_at' => $dt , 'updated_at' => $dt ]) ;
            
        }

        DB::table('earnings')->insert($tValues);

        return true;
    }

    private function addNewUserReturnId($request){
        $this->cpsDistributionValue();
        $userid = Auth::user()->id;
        $user = new User;
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->address = $request->address1;
        $user->zipcode = $request->zipcode;
        $user->mobileno = $request->mobileno;
        $user->username = $request->newUsername;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->type = 2;
        $saved = $user->save();

        $defaultFunds = new userFunds;
        $defaultFunds->userid = $user->id;
        $defaultFunds->registrationFunds = 0;
        $defaultFunds->payoutFunds = 2500;
        $defaultFunds->save();

        $referal = new referals;
        $referal->userid = $userid;
        $referal->refered = $user->id;
        $referal->save();

        $userFunds = userFunds::where('userid', $userid)->first();
        $funds = $userFunds->registrationFunds;
        $finalFunds = $funds - 1580.00;
        $userFunds->registrationFunds = $finalFunds;
        $userFunds->save();

        return $user->id;
    }

    private function splitTableMovement($groupID, $newUserID, $newSlot){
        $updateGroupMoved = DB::update('update movements set userid = '.$newUserID.'  where groupid = '.$groupID.' and movedto = '.$newSlot);

        return ($updateGroupMoved ? true : false);
    }

    private function newMovement($newUserID, $groupID){
        $updateGroupMoved = DB::update('update movements set origin = movedto, movedto = movedto -1  where groupid = '.$groupID);

        $dump = movements::where('movedto', '<', 0);
        $dump->delete();

        $newUserMovement = new movements;
        $newUserMovement->groupid = $groupID;
        $newUserMovement->userid = $newUserID;
        $newUserMovement->movedto = 13;
        $newUserMovement->origin = 0;
        $newUserMovement->active = 1;
        $newUserMovement->save();

        return ($newUserMovement ? true : false);
    }

    private function checkRegistrationFunds(){
        $userid = Auth::user()->id;

        $regfunds = DB::table('userfunds')->select('registrationFunds')->where('userid', $userid)->first();

        if($regfunds->registrationFunds >= 1580){
            return true;
        }else{
            return Redirect::back()->withErrors(['msg', 'Insufficient registration funds.']);
        }
    }

    private function currentGroup(){
        $userid = Auth::user()->id;
        
        $currentGroup = DB::table('movements as m')
                            ->select('m.id', 'm.groupid', DB::raw('MAX(t.tierid) as tid'))
                            ->leftJoin('tiergroups as t', 't.id', '=', 'm.groupid')
                            ->where('m.userid', $userid)
                            ->orderBy('m.id', 'desc')
                            ->groupBy('t.tierid', 'm.id','m.groupid')
                            ->first();

        if($currentGroup){
            return $currentGroup->groupid;
        }else{
            return false;
        }
         
    }

    private function currentGroup2(){
        $userid = Auth::user()->id;
        
        $currentGroup = DB::table('movements as m')
                            ->select('m.id', 'm.groupid', DB::raw('MAX(t.tierid) as tid'))
                            ->leftJoin('tiergroups as t', 't.id', '=', 'm.groupid')
                            ->where('m.userid', $userid)
                            ->orderBy('t.tierid', 'desc')
                            ->groupBy('t.tierid', 'm.id','m.groupid')
                            ->first();

        if($currentGroup){
            return $currentGroup->groupid;
        }else{
            return false;
        }
         
    }

    private function getCurrentTable($groupid){
        $currentTable = DB::table('tiergroups')->where('id', $groupid)->first();

        return $currentTable->tierid;
    }

    private function getFunds($userid, $column){
        $regFunds = DB::table('userfunds')
                        ->where('userid', $userid)
                        ->first();

        if(!$regFunds){
            userFunds::create([
                'userid' => $userid,
                'registrationFunds' => 0,
                'payoutFunds' => 0
            ]);

            $this->getFunds($userid, $column);  
        }else{
            if($column == 'registrationFunds'){
                return $regFunds->registrationFunds;
            }elseif($column == 'payoutFunds'){
                return $regFunds->payoutFunds;
            }
        }
        
    }

    public function getGroupName($groupID){
        $groupName = DB::table('tiergroups')->where('id', $groupID)->first();

        return $groupName->groupname;
    }

    public function getGroupTier($groupID){
        $groupName = DB::table('tiergroups')->where('id', $groupID)->first();

        return $groupName->tierid;
    }

    private function newTierGroups($groupID, $tier){
        $userid = Auth::user()->id;
        $leftTable = [2,4,5,8,9,10];
        $rightTable = [3,6,7,11,12,13];

        $originName = $this->getGroupName($groupID);
        $originTier = $this->getGroupTier($groupID);
        $tierplus = (int)$originTier + 1;

        $advanceTierGroup = new tiergroups;
        $advanceTierGroup->groupid = 1;
        $advanceTierGroup->groupname = $originName.' Table '.$tierplus;
        $advanceTierGroup->tierid = $tierplus;
        $advanceTierGroup->parentid = 0;
        $advanceTierGroup->ancestor = $groupID;
        $advanceTierGroup->left = 0;
        $advanceTierGroup->right = 0;
        $advanceTierGroup->status = 'moving';
        $advanceTierGroup->save();

        if($advanceTierGroup){

            $slotOneID = $this->getSlotOne($groupID);
            $this->newMovement($slotOneID, $advanceTierGroup->id);
            $this->iAdvancePerks($slotOneID, $tier);

        }

        //extra shit
        $freeGroup = new tiergroups;
        $freeGroup->groupid = 1;
        $freeGroup->groupname = $originName.'Free Table';
        $freeGroup->tierid = $originTier;
        $freeGroup->parentid = 0;
        $freeGroup->ancestor = $advanceTierGroup->id;
        $freeGroup->left = 0;
        $freeGroup->right = 0;
        $freeGroup->status = 'moving';
        $freeGroup->save();

        if($freeGroup){

            $slotOneID = $this->getSlotOne($groupID);
            $freeAccount = $this->addFreeAccount($slotOneID, 'C',  $originTier);
            $this->newMovement($freeAccount, $freeGroup->id);

        }
        //


        $leftTierGroup = new tiergroups;
        $leftTierGroup->groupid = 1;
        $leftTierGroup->groupname = $originName.'(L)';
        $leftTierGroup->tierid = (int)$originTier;
        $leftTierGroup->parentid = $groupID;
        $leftTierGroup->ancestor = $advanceTierGroup->id;
        $leftTierGroup->left = 1;
        $leftTierGroup->right = 0;
        $leftTierGroup->status = 'moving';
        $leftTierGroup->save();

        if($leftTierGroup){
            $leftID = $leftTierGroup->id;

            for($i = 0; $i < count($leftTable); $i++){
                $getM = $this->getMovement($groupID, $leftTable[$i]);
                $pushM = $this->newSplitMovement($getM, $leftID, $i+1);
            }

            $freeAccountLeft = $this->addFreeAccount($slotOneID, 'A', $originTier);
            $pushM = $this->newSplitMovement($freeAccountLeft, $leftID, 7);
            $this->addSplitSlots($leftID);
        }

        $rightTierGroup = new tiergroups;
        $rightTierGroup->groupid = 1;
        $rightTierGroup->groupname = $originName.'(R)';
        $rightTierGroup->tierid = (int)$originTier;
        $rightTierGroup->parentid = $groupID;
        $rightTierGroup->ancestor = $advanceTierGroup->id;
        $rightTierGroup->left = 0;
        $rightTierGroup->right = 1;
        $rightTierGroup->status = 'moving';
        $rightTierGroup->save();

        if($rightTierGroup){
            $rightID = $rightTierGroup->id;

            for($i = 0; $i < count($leftTable); $i++){
                $getM = $this->getMovement($groupID, $rightTable[$i]);
                $pushM = $this->newSplitMovement($getM, $rightID, $i+1);
            }
            $freeAccountRight = $this->addFreeAccount($slotOneID, 'B', $originTier);
            $pushM = $this->newSplitMovement($freeAccountRight, $rightID, 7);
            $this->addSplitSlots($rightID);
        }
        $res = [$leftID, $rightID];
        return $res;
    }

    private function newGroups($groupID, $tier){
        $userid = Auth::user()->id;
        $leftTable = [2,4,5,8,9,10];
        $rightTable = [3,6,7,11,12,13];

        $originName = $this->getGroupName($groupID);
        $originTier = $this->getGroupTier($groupID);
        $ancestor = $this->getGroupAncestor($groupID);
        $tierplus = (int)$originTier + 1;
        $slotOneID = $this->getSlotOne($groupID);

        //extra shit
        $freeGroup = new tiergroups;
        $freeGroup->groupid = 1;
        $freeGroup->groupname = $originName.'Free Table';
        $freeGroup->tierid = $originTier;
        $freeGroup->parentid = 0;
        $freeGroup->left = 0;
        $freeGroup->right = 0;
        $freeGroup->ancestor = $ancestor;
        $freeGroup->status = 'moving';
        $freeGroup->save();

        if($freeGroup){

            $freeAccount = $this->addFreeAccount($slotOneID, 'C', $originTier);
            $this->newMovement($freeAccount, $freeGroup->id);

        }
        //

        $leftTierGroup = new tiergroups;
        $leftTierGroup->groupid = 1;
        $leftTierGroup->groupname = $originName.'(left)';
        $leftTierGroup->tierid = (int)$originTier;
        $leftTierGroup->parentid = $groupID;
        $leftTierGroup->ancestor = $ancestor;
        $leftTierGroup->left = 1;
        $leftTierGroup->right = 0;
        $leftTierGroup->status = 'moving';
        $leftTierGroup->save();

        if($leftTierGroup){
            $leftID = $leftTierGroup->id;

            for($i = 0; $i < count($leftTable); $i++){
                $getM = $this->getMovement($groupID, $leftTable[$i]);
                $pushM = $this->newSplitMovement($getM, $leftID, $i+1);
            }

            $freeAccountLeft = $this->addFreeAccount($slotOneID, 'A', $originTier);
            $pushM = $this->newSplitMovement($freeAccountLeft, $leftID, 7);
            $this->addSplitSlots($leftID);
        }

        $rightTierGroup = new tiergroups;
        $rightTierGroup->groupid = 1;
        $rightTierGroup->groupname = $originName.'(right)';
        $rightTierGroup->tierid = (int)$originTier;
        $rightTierGroup->parentid = $groupID;
        $rightTierGroup->ancestor = $ancestor;
        $rightTierGroup->left = 0;
        $rightTierGroup->right = 1;
        $rightTierGroup->status = 'moving';
        $rightTierGroup->save();

        if($rightTierGroup){
            $rightID = $rightTierGroup->id;

            for($i = 0; $i < count($rightTable); $i++){
                $getM = $this->getMovement($groupID, $rightTable[$i]);
                $pushM = $this->newSplitMovement($getM, $rightID, $i+1);
            }

            $freeAccountRight = $this->addFreeAccount($slotOneID, 'B', $originTier);
            $pushM = $this->newSplitMovement($freeAccountRight, $rightID, 7);
            $this->addSplitSlots($rightID);
        }

        $parentID = $this->getParentId($ancestor);
        if($parentID > 0){
            $groupCount = $this->getGroupCount($ancestor);
            $newSlot = $groupCount + 1;
            $this->splitTableMovement($ancestor, $slotOneID, $newSlot);
        }else{
            $this->newMovement($slotOneID, $ancestor);
        }                
        $this->checkAncestorFull($ancestor);
        $this->iAdvancePerks($slotOneID, $tier);

        $res = [$leftID, $rightID];
        return $res;
    }

    private function addLeftTable(){

    }

    private function addRightTable(){

    }

    private function getMovement($groupID, $slot){
        $movement = DB::table('movements')->where('groupID', $groupID)->where('movedto', $slot)->first();

        return $movement->userid;
    }

    private function newSplitMovement($userid, $newGroupID, $slot){

        $newMovement = new movements;
        $newMovement->groupid = $newGroupID;
        $newMovement->userid = $userid;
        $newMovement->movedto = $slot;
        $newMovement->origin = 0;
        $newMovement->active = 1;
        $newMovement->save();
    }

    private function addSplitSlots($groupid){
        $extraSlots = 13;

        for($g = 8; $g <= $extraSlots; $g++){
            $newMovement = new movements;
            $newMovement->groupid = $groupid;
            $newMovement->userid = 0;
            $newMovement->movedto = $g;
            $newMovement->origin = 0;
            $newMovement->active = 1;
            $newMovement->save();
        }
    }

    private function userTables($userid){
        $groups = DB::table('movements')->select(DB::raw('DISTINCT(groupid)'))->where('userid', $userid)->get();

        return $groups;
    }

    private function addToInvites($userid, $target){
        $ati = new invites;
        $ati->inviter = $userid;
        $ati->target = $target;
        $ati->save();
    }

    private function addEarnings($userid, $earningType){
        $payout = userFunds::where('userid', $userid)->first();

        if($payout){
           $payout->payoutFunds += 50;
            $payout->save(); 
        }else{
            $payout = new userFunds;
            $payout->userid = $userid;
            $payout->registrationFunds = 0;
            $payout->payoutFunds = 50;
            $payout->save();
        }

        $earning = new earnings;
        $earning->userid = $userid;
        $earning->earningTypeId = $earningType;
        $earning->amount = 50;
        $earning->save();
    }

    private function addIndirect($userid){
        $indirect = invites::where('target', $userid)->first();
        if($indirect){
            $indirectCount = $this->getIndirects($indirect->inviter);
                if($indirectCount < 11){
                    $payout = userFunds::where('userid', $indirect->inviter)->first();

                    if($payout){
                       $payout->payoutFunds += 5;
                        $payout->save(); 
                    }else{
                        $payout = new userFunds;
                        $payout->userid = $indirect->inviter;
                        $payout->registrationFunds = 0;
                        $payout->payoutFunds = 5;
                        $payout->save();
                    }

                    $earning = new earnings;
                    $earning->userid = $indirect->inviter;
                    $earning->earningTypeId = 8;
                    $earning->amount = 5;
                    $earning->save();
                }
        }
    }

    private function getAccumulated($userid){
        $accumulated = DB::table('earnings')->select(DB::raw('SUM(amount) as total'))->where('userid', $userid)->first();

        return $accumulated->total;
    }

    private function iAdvancePerks($userid, $tier){
        $advanceAmount = $this->perksRewards[$tier];
        $regedAccount = DB::table('sys_accounts')->where('userid', $userid)->count();
        if($regedAccount < 1 && $tier == 1){
            $advanceAmount = 1500;
        }

        $payout = userFunds::where('userid', $userid)->first();
        if($payout){
           $payout->payoutFunds += $advanceAmount;
            $payout->save(); 
        }else{
            $payout = new userFunds;
            $payout->userid = $userid;
            $payout->registrationFunds = $tier;
            $payout->payoutFunds = $advanceAmount;
            $payout->save();
        }

        $earning = new earnings;
        $earning->userid = $userid;
        $earning->earningTypeId = (int)$tier+1;
        $earning->amount = $advanceAmount;
        $earning->save();
    }

    private function getGroupAncestor($groupID){
        $ancestor = tiergroups::find($groupID);

        return $ancestor->ancestor;
    }

    private function getSlotOne($groupid){
        $slotOne = movements::where('groupid', $groupid)->where('movedto', 1)->first();

        return $slotOne->userid;
    }

    private function getAllGroups(){
        $userid = Auth::user()->id;
        $groupsT = [0, 0, 0, 0, 0, 0, 0];

            $groups = DB::table('movements as m')->select(['t.tierid', 'm.groupid'])->distinct('groupid')->leftJoin('tiergroups as t', 'm.groupid', '=', 't.id')->where('userid', $userid)->get();

            foreach ($groups as $group) {
                $groupsT[$group->tierid] = $group->groupid;
            }

        return $groupsT;
    }

    private function addFreeAccount($ownerID, $suffix, $tier){
        $owner = User::find($ownerID);
        $nfa = DB::table('sys_accounts')->where('owner', $ownerID)->count();
        $nfaCount = (int)$nfa + 1;

        $user = new User;
        $user->firstname = $owner->firstname;
        $user->middlename = $owner->middlename;
        $user->lastname = $owner->lastname;
        $user->address = $owner->address;
        $user->zipcode = $owner->zipcode;
        $user->mobileno = $owner->mobileno;
        $user->username = $owner->username.'('.$suffix.''.$tier.')';
        $user->password = $owner->password;
        $user->type = 2;
        $user->email = $owner->email;
        $saved = $user->save();

        $sysAcc = DB::table('sys_accounts')->insert(['userid' => $user->id, 'owner' => $ownerID]);

        // $userFunds = userFunds::find($userid);
        // $userFunds->registrationFunds -= 1580;
        // $userFunds->save();

        return $user->id;
    }

    private function getIndirects($userid){
        $indirects = earnings::where('userid', $userid)->where('earningTypeId', 8)->count();

        return ($indirects ? $indirects : false);
    }

    private function checkAncestorFull($groupID){

        $groupCount = $this->getGroupCount($groupID);
        $tier = $this->getCurrentTable($groupID);
        $parentID = $this->getParentId($groupID);

        if($parentID > 0){
            if($groupCount >= 13){
                $res = $this->newGroups($groupID, $tier);

                DB::table('tiergroups')
                ->where('ancestor', $groupID)
                ->where('tierid', (int)$tier - 1)
                ->where('left', 1)
                ->update(['ancestor' => $res[0]]);

                DB::table('tiergroups')
                ->where('ancestor', $groupID)
                ->where('tierid', (int)$tier - 1)  
                ->where('right', 1)
                ->update(['ancestor' => $res[1]]);
            }
        }else{
            if($groupCount >= 13){
                $res = $this->newTierGroups($groupID, $tier);

                DB::table('tiergroups')
                ->where('ancestor', $groupID)
                ->where('tierid', (int)$tier - 1)
                ->where('left', 1)
                ->update(['ancestor' => $res[0]]);

                DB::table('tiergroups')
                ->where('ancestor', $groupID)
                ->where('tierid', (int)$tier - 1)  
                ->where('right', 1)
                ->update(['ancestor' => $res[1]]);
            }
        }  
    }

    private function isGenerated($userid){
        $account = DB::table('sys_accounts')->where('userid', $userid)->count();

        return ($account > 0 ? true : false);
    }

    private function getMyAccounts($userid){
        $accounts = DB::table('sys_accounts as s')
                    ->select('u.username')
                    ->leftJoin('users as u', 'u.id', '=', 's.userid')
                    ->where('owner', $userid)->get();

        return $accounts;
    }

    private function getEnchasmentMethod(){
        $methods = DB::table('encashmentmethods')->select('id', 'name', 'type')->get(); 
        return $methods;
    }

    private function itexmo($number,$message){
            $ch = curl_init();
            $itexmo = array('1' => $number, '2' => $message, '3' => 'PR-MAXH5706_EHRDC');
            curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
            curl_setopt($ch, CURLOPT_POST, 1);
             curl_setopt($ch, CURLOPT_POSTFIELDS, 
                      http_build_query($itexmo));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            return curl_exec ($ch);
            curl_close ($ch);
            //  $itexmo = array('1' => $number, '2' => $message, '3' => 'PR-MAXH5706_EHRDC');
            //  $url = 'https://www.itexmo.com/php_api/api.php';
            // $param = array(
            //     'http' => array(
            //         'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            //         'method'  => 'POST',
            //         'content' => http_build_query($itexmo),
            //     ),
            // );
            // $context  = stream_context_create($param);
            // return file_get_contents($url, false, $context);
    }
    public function encashment(Request $request){
        // return $request->amount;
        $userid = Auth::user()->id;
        $funds = $this->getFunds($userid, 'payoutFunds');
        $check = DB::table('earnings')->where('userid', $userid)->where('earningTypeId', 2)->first();
        $amount = $request->amount;
        $method = (int)$request->name;
        $type = DB::table('encashmentmethods')->where('id', $method)->first();
        $checktype = $type->type;

        $lastamount = str_replace(',', '', $amount);
        $finalamount = (float)$lastamount;
        $recipient = $request->recipient;
        $checkRecipient = DB::table('recipients')->where('userid', $userid)->first();
        $checkbnkacnt = DB::table('bank_accounts')->where('userid', $userid)->first();
        if ($checktype == 1) {  
            //check bankaccount

            if (is_null($check)) {
                return "You're not allowed to encash.";
            }else{
                if(is_null($checkbnkacnt)){
                    return "Please provide your bank account.";
                }else{
                    if ($checkbnkacnt->name && $checkbnkacnt->number) {
                        if ($finalamount < 0) {
                                return "Your encashment request is not allowed. Please enter correct figure.";
                        }else{
                            if ($finalamount >= 500) {
                                # code...
                           
                                $adminfee = $finalamount * 0.06;
                                $tax = $finalamount * 0.10;

                                 $charge = $adminfee + $tax;
                                 $netamount = $finalamount - $charge;
                                // $withtaxamount = $totalamount + $tax;
                                if ($finalamount <= $funds) {

                                    //Remember to change this with your cluster name.
                                    $options = array(
                                        'cluster' => 'ap1', 
                                        'encrypted' => true
                                    );
                             
                                   //Remember to set your credentials below.
                                    // $pusher = new Pusher(   
                                    //     'd8942b63c409c04614b8',
                                    //     '5ec8f0059698f65a507c',
                                    //     '672463',
                                    //     $options
                                    // );

                                    $encash = new encashments;
                                    
                                    $encash->userid = $userid;
                                    $encash->amount = $finalamount;
                                    $encash->encashmentmethod = $method;
                                    $encash->net = $netamount;
                                    $encash->transactionmethod = 1;
                                    $encash->status = 1;
                                    $encash->recipient = $recipient;
                                    $encash->save();
                                    $encashid = $encash->id;
                                    // return 'heheh';
                                    $encashmentId = $encash->id;
                                    $finalFunds = $funds - $finalamount;
                                    $updateFunds = userFunds::where('userid', $userid)->first();
                                    $updateFunds->payoutFunds = $finalFunds;
                                    $updateFunds->save();
                                    $adminFee = new admin_fee;
                                    $adminFee->amount = $adminfee;
                                    $adminFee->userid = $userid;
                                    $adminFee->encashment_id = $encashmentId;
                                    $adminFee->save();
                                    $ctax = new tax;
                                    $ctax->amount = $tax;
                                    $ctax->userid = $userid;
                                    $ctax->encashment_id = $encashmentId;
                                    $ctax->save();

                                    // $encash->id->notify(new NotifyAdmin($encash->id));
                                    // $getdate = encashments::find($encashid)->first();
                                    // $date = $getdate->created_at;
                                    // $modepayment = encashmentmethod::find($method);
                                    // $modename = $modepayment->name;
                                    // $mobileno = Auth::user()->mobileno;
                                    // $name = Auth::user()->firstname; 
                                    // $lastname = Auth::user()->lastname;
                                    // $message = 'ENCASHENT:'.$modename.' SENDER: Jenny Lou C. Gutierrez RECEIVER:'.$name.' '.$lastname.' TRANSACTION DATE:'.$date.' AMOUNT:'.$finalamount;
                                    // $this->itexmo($mobileno,$message);

                                    $userlogin = Auth::id();
                                    $encashmentmethod = encashmentmethod::find($method);
                                    $methodname = $encashmentmethod->name;
                                    // dd($userlogin);

                                    $getuser = User::find($userlogin);
                                    $firstname = $getuser->firstname;
                                    $lastname = $getuser->lastname;
                                    $content = '<b>'.$firstname.' '.$lastname.'</b> request encashment via <b>'.$methodname.'</b> with an amount of '.$finalamount.' Pesos';

                                    $userinit1 = substr($getuser->firstname,0,1);
                                    $userinit2 = substr($getuser->lastname,0,1);
                                    $fullinit = $userinit1.$userinit2;

                                    $saveNotif = new NotificationLog;
                                    $saveNotif->content = $content;
                                    $saveNotif->has_read = 0;
                                    $saveNotif->user_id = $userlogin;
                                    $saveNotif->user = $fullinit;
                                    $saveNotif->save();
                                    // $datenow = Carbon::now()
                                    $datecreated = date("Y-m-d H:i:s", strtotime($saveNotif->created_at));
                                    $time = $this->gettimestamp($datecreated);
                                    // $getcontent = NotificationLog::find($saveNotif);
                                    $message = [];
                                    $message[0] = $saveNotif->content;
                                    $message[1] = 1;
                                    $message[2] = $time;
                                    $message[3] = $fullinit;
                                    
                                    // $news = 1;
                                    
                                    //Send a message to notify channel with an event name of notify-event
                                    $pusher->trigger('notify', 'notify-event', $message);

                                    
                                    return "Your Encashment Request has been sent and you will be notified once approved. Thank you.";
                                }else{
                                    return "Insufficient Earnings.";
                                }
                            }else{
                                // return $finalamount;
                                return "Your encashment request is not allowed.";
                            }
                        }
                    }else{
                        return "Please provide your account name and account number.";
                    }
                }
                
                
            }//end of checking bank account
        }else{
            if (!is_null($check)) {
                if(!is_null($checkRecipient)){
                    if ($finalamount < 0) {
                            return "Your encashment request is not allowed. Please enter correct figure.";
                    }else{
                        if ($finalamount >= 500) {
                            # code...
                       
                            $adminfee = $finalamount * 0.06;
                            $tax = $finalamount * 0.10;

                             $charge = $adminfee + $tax;
                             $netamount = $finalamount - $charge;
                            if ($finalamount <= $funds) {

                                 $encash = new encashments;
                                $encash->userid = $userid;
                                $encash->amount = $finalamount;
                                $encash->encashmentmethod = $method;
                                $encash->net = $netamount;
                                $encash->transactionmethod = 1;
                                $encash->status = 1;
                                $encash->recipient = $recipient;
                                $encash->save();
                                $encashid = $encash->id;
                                $encashmentId = $encash->id;
                                $finalFunds = $funds - $finalamount;
                                $updateFunds = userFunds::where('userid', $userid)->first();
                                $updateFunds->payoutFunds = $finalFunds;
                                $updateFunds->save();
                                $adminFee = new admin_fee;
                                $adminFee->amount = $adminfee;
                                $adminFee->userid = $userid;
                                $adminFee->encashment_id = $encashmentId;
                                $adminFee->save();
                                $ctax = new tax;
                                $ctax->amount = $tax;
                                $ctax->userid = $userid;
                                $ctax->encashment_id = $encashmentId;
                                $ctax->save();

                                // $encash->id->notify(new NotifyAdmin($encash->id));
                                // $getdate = encashments::find($encashid)->first();
                                // $date = $getdate->created_at;
                                // $modepayment = encashmentmethod::find($method);
                                // $modename = $modepayment->name;
                                // $mobileno = Auth::user()->mobileno;
                                // $name = Auth::user()->firstname; 
                                // $lastname = Auth::user()->lastname;
                                // $message = 'ENCASHENT:'.$modename.' SENDER: Jenny Lou C. Gutierrez RECEIVER:'.$name.' '.$lastname.'TRANSACTION DATE:'.$date.' AMOUNT:'.$finalamount;
                                // $this->itexmo($mobileno,$message);
                                $username = Auth::user()->username;
                                $description = $username.' request for encashment an amount of '.$request->amount;
                                $this->activityLogs($description, $userid, $encashid, 1);
                                return "Your Encashment Request has been sent and you will be notified once approved. Thank you.";
                            }else{
                                return "Insufficient Earnings.";
                            }
                        }else{
                            // return $finalamount;
                            return "Your encashment request is not allowed.";
                        }
                    }
                }else{
                    return "Please Provide Your Remittance Receiver's Details.";
                }
            }else{
                return "You're not allowed to encash.";
            }
        }
    }
    private function activityLogs($description, $userid, $transactionid, $type){
        $logs = new activity_logs;
        $logs->description = $description;
        $logs->by  = $userid;
        $logs->transaction_id = $transactionid;
        $logs->type = $type;
        $logs->save();
        return 'sucess';
    }
    public function convertRegFund(Request $request){
        $userid = Auth::user()->id;
        $regFunds = $this->getFunds($userid, 'registrationFunds');
        $funds = $this->getFunds($userid, 'payoutFunds');
        $amount = $request->amount;
        $lastamount = str_replace(',', '', $amount);
        $finalamount = (float)$lastamount;
        if ($finalamount < 0) {
            return "Your encashment request is not allowed. Please enter correct figure.";
        }else{
            $adminfee = $finalamount * 0.06;
            $totalamount = $finalamount + $adminfee;
            if ($totalamount <= $funds) {
                $encash = new encashments;
                $encash->userid = $userid;
                $encash->amount = $finalamount;
                $encash->encashmentmethod = 0;
                $encash->status = 1;
                $encash->net = $finalamount;
                $encash->transactionmethod = 2;
                $encash->save();
                $encashmentId = $encash->id;

                $finalFunds = $funds - $totalamount;
                $updateFunds = userFunds::where('userid', $userid)->first();
                $updateFunds->payoutFunds = $finalFunds;
                $updateFunds->save();

                $adminFee = new admin_fee;
                $adminFee->amount = $adminfee;
                $adminFee->userid = $userid;
                $adminFee->encashment_id = $encashmentId;
                $adminFee->save();

                $user = User::find($userid);
                // $user = ;
                // return $user;

                User::where('type', 1)->first()->notify(new NotifyAdmin($user));

                return "Your Registration Funds conversion request has been successfully sent and you will be notified once approved. Thank you.";
            }else{
                return 'Insufficient Funds';
            }
        }
    }
    private function getRecipient(){
        $userid = Auth::user()->id;
        $recipient = DB::table('recipients')->where('userid', $userid)->first(); 
        return $recipient;
    }
}

    // 0 - monthly sharing pool
    // 1 - advance 1
    // 2 - advance 2
    // 3 - advance 3
    // 4 - advance 4
    // 5 - advance 5
    // 6 - advance 6
    // 7 - indirect referal
    // 8 - direct referral
    // 9 - MPS
    // 10 - CPS