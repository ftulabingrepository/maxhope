@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>Funds</div>
      </h1>
       
        <!--accounts area-->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="ion ion-card"></i> Funds</h4>
                    <a href="{{route('userfunds')}}" class="btn btn-primary btn-add" style="border-radius: 5px;">Edit User Funds</a>
                </div>
                <div class="card-body">
                    @if(session()->has('message'))
                      <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                         {{ session()->get('message') }}
                      </div>
                    @elseif(session()->has('error'))
                      <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ session()->get('error') }}
                      </div>
                    @endif
                    <form method="POST" action="{{ url('maintenance/funds/add') }}">
                        @csrf
                        <label class="label" for="useraccount">Account/Username</label>
                        <input list="users" name="useraccount" style="width: 450px; max-width: 100%;" class="form-control" placeholder="Select Username" id="answer" required>
                        <datalist id="users">

                            @foreach($userslist as $user)
                                <option data-value="{{$user->id}}">{{$user->username}}</option>
                            @endforeach
                        </datalist>
                        <input type="hidden" name="user" id="answer-hidden">
                        <label class="label" for="amount">Funds</label>
                        <input class="form-control btn-lg" name="amount" id="name" type="number" placeholder="Add Funds">
                        <p class="note">Please enter amount not less than &#8369;1,580.00.</p>
                        <div class="clearfix">
                            <div class="col-xs-6">
                                <button class="btn btn-primary btn-block" style="margin-top: 10px;" type="submit">Add Registration Fund</button>
                            </div>
                            
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
</div>

@endsection
@section('customjs')
<script src="{{asset('js/maintenance/funds.js')}}"></script>
@endsection