@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>Encashment Method</div>
        <a href="{{route('encashmentMethodAdd')}}" class="btn btn-primary btn-add">Add</a>
      </h1>
       
        <!--accounts area-->
        
        <!--end accounts area-->
        <!--tables area-->
        <div class="card">
            <div class="card-body">
                
                
                <table class="table table-bordered dataTable" id="tbl-encashmentmethod" style="border-collapse: collapse!important;">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
   
</div>
<div class="bootbox bootbox-confirm modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Are you sure you want to delete this record ?
        <input type="hidden" name="methodId" value="">

      </div> 
      <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal" id="deleteCancel">Cancel</button>
        <button type="button" data-bb-handler="confirm" class="btn btn-sm btn-primary" id="deleteNow">OK</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('customjs')
<script src="{{asset('js/encashmentmethod/encashmentmethod.js')}}"></script>
@endsection