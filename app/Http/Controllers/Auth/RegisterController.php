<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\userFunds;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('access');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'middlename' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'zipcode' => 'required|integer',
            'username' => 'required|string|max:255|unique:users',
            'mobileno' => 'required',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {   
        // return User::create([
        //     'firstname' => $data['firstname'],
        //     'middlename' => $data['middlename'],
        //     'lastname' => $data['lastname'],
        //     'suffix' => $data['suffix'],
        //     'address' => $data['address'],
        //     'zipcode' => $data['zipcode'],
        //     'username' => $data['username'],
        //     'mobileno' => $data['mobileno'],
        //     'email' => $data['email'],
        //     'type' => 2,
        //     'password' => Hash::make($data['password']),
        // ]);
        $users = new User;
        $users->firstname = $data['firstname'];
        $users->middlename = $data['middlename'];
        $users->lastname = $data['lastname'];
        $users->suffix = $data['suffix'];
        $users->address = $data['address'];
        $users->zipcode = $data['zipcode'];
        $users->username = $data['username'];
        $users->mobileno = $data['mobileno'];
        $users->email = $data['email'];
        $users->type = 2;
        $users->password = Hash::make($data['password']);
        $users->save();
        $user = $users->id;
        $funds = new userFunds;
        $funds->userid = $user;
        $funds->registrationFunds = 0;
        $funds->payoutFunds = 2500;
        $funds->save();
        return $users;

    }
}
