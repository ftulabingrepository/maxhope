<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModulesAccess extends Model
{
      protected $table = 'modules_accesses';
    protected $fillable = [
        'usertype', 'module_id','grant_access'
    ];
}
