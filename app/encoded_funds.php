<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class encoded_funds extends Model
{
    protected $table = 'encoded_funds'; 
    protected $fillable = [
        'userid', 'amount'
    ];
}
