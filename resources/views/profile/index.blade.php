@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>PROFILE</div>
      </h1>
       
        <!--accounts area-->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="ion ion-card"></i> Funds</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('maintenance/funds/add') }}">
                        @csrf
                        <label class="label" for="useraccount">Account</label>
                        <input list="users" name="useraccount" style="width: 450px; max-width: 100%;" class="form-control" placeholder="Select Member" id="answer">
                        <datalist id="users">

                            @foreach($userslist as $user)
                                <option data-value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </datalist>
                        <input type="hidden" name="user" id="answer-hidden">
                        <label class="label" for="amount">Funds</label>
                        <input class="form-control btn-lg" name="amount" id="name" type="number" placeholder="Add Funds">
                        <div class="clearfix">
                            <div class="col-xs-6">
                                <button class="btn btn-primary btn-block" style="margin-top: 10px;" type="submit">Add Encashment Method</button>
                            </div>
                            
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
</div>

@endsection
@section('customjs')

@endsection