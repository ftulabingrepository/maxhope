<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user()->id;
        $name = Auth::user()->firstname;
        $lastname = Auth::user()->lastname;
        return view('home', compact('name', 'lastname'));
    }
    public function logout(Request $request) {
      Auth::logout();
      return redirect('/');
    }
}
