<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class activity_logs extends Model
{
    protected $table = 'activity_logs'; 
    protected $fillable = [
        'description', 'by', 'transaction_id', 'type'
    ];
}
