$(document).ready(function(){
	var payout = $('#tbl-accounts').DataTable({
		processing: true,
		serverside: true,
		ajax: 'getaccounts',
		columns: [
			{data: 'id', name: 'id'},
			{data: 'username', name: 'username'},
			{data: 'firstname', name: 'firstname'},
			{data: 'middlename', name: 'middlename'},
			{data: 'lastname', name: 'lastname'},
			{data: 'created_at', name: 'create_at'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
	});
});