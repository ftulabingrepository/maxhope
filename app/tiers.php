<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tiers extends Model
{
    protected $table = 'tiers'; 
    protected $fillable = [
        'tiername', 'levels'
    ];
}
