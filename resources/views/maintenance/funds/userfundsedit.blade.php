@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>User Funds</div>
      </h1>
       
        <!--accounts area-->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card" style="min-height: 350px;">
                <div class="card-header">
                    <h4><i class="ion ion-card"></i> User Funds</h4>
                </div>
                <div class="card-body">
                    @if(session()->has('message'))
                      <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                         {{ session()->get('message') }}
                      </div>
                    @elseif(session()->has('error'))
                      <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong>  {{ session()->get('error') }}
                      </div>
                    @endif
                    <form method="POST" action="{{ url('maintenance/user/funds') }}">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-md-4">
                                 <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" name="firstname" value="{{$user->username}}" class="form-control" readonly>
                                </div> 
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="firstname" value="{{$user->firstname}}" class="form-control" readonly>
                                </div> 
                            </div>
                            <div class="col-12 col-md-4">
                                 <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="lastname" value="{{$user->lastname}}" class="form-control" readonly>
                                </div>   
                            </div>
                        </div>    
                   
                    </form>
                    <div class="row">
                        <div class="col-12 col-md-4">  
                             <table class="table summary-tbl">
                                <tbody>
                                    @if(is_null($sysgen))
                                        @if(is_null($exit))
                                         <tr>
                                             <td><i class="ion ion-ios-circle-filled"></i> iAdvance Perks Table-1</td>
                                             <td style="text-align: right;">&#8369; 2,500.00</td>
                                         </tr>
                                        @endif
                                    @endif
                                    @foreach($earnings as $earning)
                                    <tr>
                                        <td><i class="ion ion-ios-circle-filled"></i> {{$earning->type}}</td>
                                        @if($earning->tid == 2)
                                            @if(is_null($sysgen))
                                                <td style="text-align: right;">&#8369;{{number_format((int)$earning->total + 2500 , 2)}}</td>
                                            @else
                                                <td style="text-align: right;">&#8369;{{number_format($earning->total, 2)}}</td>
                                            @endif
                                           
                                        @else
                                        <td style="text-align: right;">&#8369;{{number_format($earning->total, 2)}}</td>
                                        @endif
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td>TOTAL</td>
                                        @if(is_null($sysgen))
                                            <td style="text-align: right;">&#8369;{{number_format($total +2500, 2)}}</td>
                                        @else
                                            <td style="text-align: right;">&#8369;{{number_format($total, 2)}}</td>
                                        @endif
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-12 col-md-8">
                            
                                <form method="POST" action="submit">
                                    @csrf
                                    <div class="row">
                                    <div class="col-12 col-md-6">  
                                        <div class="form-group">
                                            <h3 class="primary-color">Registration Funds</h3>
                                            <input type="number" name="registrationFunds" value="{{$user->registrationFunds}}" class="form-control" step="any">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">  
                                        <h3 class="primary-color">Payout Funds</h3>
                                        <div class="form-group">
                                            <input type="number" name="payoutFunds" value="{{$user->payoutFunds}}" class="form-control" step="any">
                                        </div>
                                    </div>
                                    
                                    
                                    </div>
                                    <button type="submit" class="btn btn-xs btn-primary btn-add">Submit</button>
                                </form>
                            
                        </div>
                        
                    </div>
                </div>
              </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
</div>

@endsection
@section('customjs')
<script src="{{asset('js/maintenance/userfunds.js')}}"></script
@endsection