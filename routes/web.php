<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::group(['middleware' => ['access']], function () {

Route::get('logout', 'HomeController@logout')->name('logout');
Route::get('dashboard', 'UserController@dashboard')->name('dashboard');
Route::get('profile', 'ProfileController@view')->name('profile');
Route::post('profile/update', 'ProfileController@update');
Route::post('profile/edit', 'ProfileController@edit');
Route::post('profile/recipient', 'ProfileController@recipientAdd');
Route::post('profile/recipient/edit', 'ProfileController@recipientEdit');
Route::post('addAccount', 'UserController@userRegFunds');
Route::get('createGroup', 'GroupController@createGroup')->name('createGroup');
Route::post('addGroup', 'GroupController@addGroup');
Route::get('earningssummary', 'EarningsSummaryController@view')->name('earningssummary');
Route::get('payout', 'PayOutController@view')->name('payout');
Route::get('getpayout', 'PayOutController@getEncashments');
Route::get('maintenance/payout/{id}/edit', 'PayOutController@edit')->name('payouts');
Route::post('maintenance/payout/{id}/submit', 'PayOutController@editSubmit')->name('payoutedit');
Route::get('accounts', 'AccountsController@index');
Route::get('getaccounts', 'AccountsController@getAccounts');
Route::post('logoutAcc', 'AccountsController@logoutAcc')->name('logoutAcc');
Route::get('msp', 'MPSController@index');
Route::get('msp/getmsp', 'MPSController@getmps');
Route::post('createMSP', 'MPSController@addMonth')->name('createMSP');
Route::get('msp/{id}/edit', 'MPSController@edit');
route::get('msp/{id}/getqualified', 'MPSController@getqualified');
Route::post('msp/{id}/submit', 'MPSController@editMsp');
/*encashment*/
Route::get('encashment', 'UserController@encashment');
Route::post('convert', 'UserController@convertRegFund');
Route::get('home', 'HomeController@index')->name('home');
/*maintenance*/
Route::get('maintenance/encashmentmethod', 'MaintenanceController@encashmentmethod')->name('encashmentmethod');
Route::get('maintenance/getencashmentmethod', 'MaintenanceController@getEncashmentMethod');
Route::get('maintenance/encashmentmethod/add', 'MaintenanceController@encashmentMethodAdd')->name('encashmentMethodAdd');
Route::get('maintenance/encashmentmethod/{id}/edit', 'MaintenanceController@encashmentMethodEdit')->name('encashmentMethodEdit');
Route::post('maintenance/encashmentmethod/{id}/edited', 'MaintenanceController@encashmentMethodEditSubmit')->name('encashmentMethodEditSubmit');
Route::post('maintenance/encashmentmethod/delete', 'MaintenanceController@encashmentMethodDelete')->name('encashmentMethodDelete');
Route::post('maintenance/encashmentmethod/add/submit', 'MaintenanceController@encashmentMethodAddSubmit')->name('encashmentMethodAddSubmit');
Route::get('maintenance/funds', 'MaintenanceController@funds')->name('funds');
Route::post('maintenance/funds/add', 'MaintenanceController@fundsAdd')->name('fundsadd');
Route::get('maintenance/user/funds', 'MaintenanceController@userFunds')->name('userfunds');
Route::get('maintenance/user/funds/{id}/edit', 'MaintenanceController@userFundsEdit');
Route::post('maintenance/user/funds/{id}/submit', 'MaintenanceController@userFundsEditSubmit')->name('hehe');
Route::get('maintenance/user/funds/redirect', 'MaintenanceController@userFundsRedirect');
Route::get('maintenect/getusers', 'MaintenanceController@getUsers');
Route::get('maintenance/tiers', 'MaintenanceController@tiers')->name('tiers');
Route::get('maintenance/tiersdata', 'MaintenanceController@tiersdata')->name('tiersdata');
Route::get('maintenance/level', 'MaintenanceController@level')->name('level');
Route::get('maintenance/leveldata', 'MaintenanceController@leveldata');
Route::get('maintenance/earningtypes', 'MaintenanceController@earningtypes')->name('earningtypes');
Route::get('maintenance/earningtypesdata', 'MaintenanceController@earningtypesdata');
Route::get('maintenance/transactionmethod', 'MaintenanceController@transactionmethod')->name('transactionmethod');
Route::get('maintenance/user/{id}/details', 'MaintenanceController@user');
Route::get('maintenance/transactionmethoddata', 'MaintenanceController@transactionmethoddata');
Route::get('maintenance/tiergroup', 'MaintenanceController@tiergroup')->name('tiergroup');
Route::get('maintenance/tiergroupdata', 'MaintenanceController@tiergroupdata');
Route::post('maintenance/updatetiers', 'MaintenanceController@updatetiers');
Route::post('maintenance/updatelevel', 'MaintenanceController@updatelevel');
Route::post('maintenance/updateearntypes', 'MaintenanceController@updateearntypes');
Route::get('maintenance/updatetranstype', 'MaintenanceController@updatetranstype');
Route::post('maintenance/updatetiergroup', 'MaintenanceController@updatetiergroup');
Route::resource('maintenance', 'MaintenanceController');

Route::post('notification/get', 'NotificationController@get');
//logs
Route::get('logs', 'LogsController@index');
Route::get('getlogs', 'LogsController@getLogs');
});
