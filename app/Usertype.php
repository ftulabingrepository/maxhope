<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usertype extends Model
{
    //
    protected $table = 'usertypes';
    protected $fillable = [
        'type'
    ];
    public $timestamps = false;
}
