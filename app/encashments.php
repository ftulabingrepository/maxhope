<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class encashments extends Model
{
    protected $table = 'encashments'; 
    protected $fillable = [
        'userid', 'amount', 'net', 'encashmentmethod', 'status', 'transactionmethod', 'recipient', 'reference_number'
    ];
}
