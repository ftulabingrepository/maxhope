<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEncashmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encashments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid');
            $table->decimal('amount',11,2);
            $table->decimal('net',11,2)->nullable();
            $table->integer('encashmentmethod');
            $table->integer('transactionmethod');
            $table->integer('status');
            $table->integer('recipient');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encashments');
    }
}
