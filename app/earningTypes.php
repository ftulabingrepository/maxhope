<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class earningTypes extends Model
{
    protected $table = 'earning_types'; 
    protected $fillable = [
        'type', 'eAmount'
    ];
}
