<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class movements extends Model
{
   
    protected $table = 'movements';
    protected $fillable = [
        'groupid', 'userid','movedto', 'origin', 'active'
    ];

}
