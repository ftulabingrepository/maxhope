<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
   <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>MaxHOPE</title>

  <link rel="stylesheet" href="{{asset('dist/modules/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('dist/modules/ionicons/css/ionicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('dist/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css')}}">
  
  <link rel="stylesheet" href="{{asset('dist/modules/summernote/summernote-lite.css')}}">
  <link rel="stylesheet" href="{{asset('dist/modules/flag-icon-css/css/flag-icon.min.css')}}">
  <link rel="stylesheet" href="{{asset('dist/css/demo.css')}}">
  <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('dist/css/skins/darksidebar.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('dist/modules/datatables/datatables.min.css')}}" type="text/javascript">
  <link href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed|Dosis|Nanum+Gothic|Titillium+Web" rel="stylesheet">
      <script>
        window.Laravel = { csrfToken: '{{ csrf_token() }}' }
    </script>
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="ion ion-navicon-round"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="ion ion-search"></i></a></li>
          </ul>
          <!-- <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search">
            <button class="btn" type="submit"><i class="ion ion-search"></i></button>
          </div> -->
        </form>
        <ul class="navbar-nav navbar-right">
          @if(Auth::user()->type == 1)
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg" id="iconsymbol"><i class="ion ion-ios-bell-outline"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right" style="overflow-y: scroll;height: 500px;">
              <div class="dropdown-header">Notifications
                <div class="float-right">
                  <a href="#">View All</a>
                </div>
              </div>
              <div class="dropdown-list-content" style="min-height: 80px;" id="notifcontent">
                @foreach($notificationlog as $nl)
                  <a href="#" class="dropdown-item dropdown-item-unread" style="display: flex;"><span style="
                      background: #1ab394;
                      color: white;
                      width: 20%;
                      height: 40px;
                      padding: 7px;
                      text-align: center;
                      font-size: 20px;
                      border-radius: 50%;
                  ">
                  {{ $nl->user }}
                  </span><div class="dropdown-item-desc" style="margin-left: 20px">{!! $nl->content !!} <div class="time"><?php  $datecreated = date("Y-m-d H:i:s", strtotime($nl->created_at)); echo UserController::gettimestamp($datecreated);?></div></div></a>
                @endforeach
              </div>
            </div>
          </li>
          @endif
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg">
            <i class="ion ion-android-person d-lg-none"></i>
            <div class="d-sm-none d-lg-inline-block">{{Auth::user()->username}}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <a href="{{route('profile')}}" class="dropdown-item has-icon">
                <i class="ion ion-android-person"></i> Profile
              </a>
              <a href="{{route('logout')}}" class="dropdown-item has-icon">
                <i class="ion ion-log-out"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      @include('layouts.sidebar')
      
                    
                        @yield('content')
      
    </div>
  </div>

  <div class="copyright">Copyright &copy; 2018 <b style="color: #1ab394;">MaxHOPE Marketing Services.</b> All Rights Reserved <br> <a href="#" class="" data-toggle="modal" data-target="#exampleModalLong"><b style="color: #1ab394"><i class="fa fa-book"></i> Terms and Conditions</b></a></div>
  @include('tac.termsandcondition')

  <script src="{{asset('dist/modules/jquery.min.js')}}"></script>
  <script src="{{asset('dist/modules/popper.js')}}"></script>
  <script src="{{asset('dist/modules/tooltip.js')}}"></script>
  <script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
  <script src="{{asset('dist/js/sa-functions.js')}}"></script>
  <script src="{{asset('dist/modules/chart.min.js')}}"></script>
  <script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
  <!-- <script src="js/app.js"></script> -->
  <script src="{{asset('dist/js/scripts.js')}}"></script>
  <script src="{{asset('dist/js/custom.js')}}"></script>
  <script src="{{asset('js/custom.js')}}"></script>
  <script src="{{asset('js/getparent.js')}}"></script>
  <script src="{{asset('dist/modules/datatables/datatables.min.js')}}" type="text/javascript"></script>

  <!-- <script type="text/javascript" src="/js/app.js"></script> -->
  <script src="{{asset('js/pusher/pusher.js')}}"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $(document).bind("contextmenu",function(e) {
 e.preventDefault();
});

      @if(Auth::user()->type == 1)
      var pusher = new Pusher('d8942b63c409c04614b8', {
      cluster: 'ap1',
      encrypted: true
      });
      
      // ion ion-navicon-round
      //Also remember to change channel and event name if your's are different.
      var channel = pusher.subscribe('notify');
      channel.bind('notify-event', function(message) {
        console.log(message);
        if(message[1] == 1){
          $('#iconsymbol').addClass("beep");
          $('#notifcontent').prepend('<a href="#" class="dropdown-item dropdown-item-unread" style="display: flex;"><span style="background: #1ab394;color: white;width: 20%;height: 40px;padding: 7px;text-align: center;font-size: 20px;border-radius: 50%;">'+message[3]+'</span><div class="dropdown-item-desc" style="margin-left: 20px">'+message[0]+' <div class="time">'+message[2]+'</div></div></a>');

          var notiflength = $('#notifcontent > a').length;

          if(notiflength > 10){
            $('#notifcontent > a').last().remove();
          }
        }
      });
      @endif
    });
    
  </script>
  @yield('customjs')
</body>
</html>