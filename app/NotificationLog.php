<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationLog extends Model
{
    protected $table = 'notification_logs';
    protected $fillable = [
        'content', 'has_read', 'user_id'
    ];
}
