<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class msp extends Model
{
    protected $table = 'msps';
    protected $fillable = [
        'month', 'year', 'by', 'member_count', 'distributed', 'amount'
    ];
}
