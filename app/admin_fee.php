<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin_fee extends Model
{
    protected $table = 'admin_fees'; 
    protected $fillable = [
        'userid', 'amount', 'encashment_id'
    ];
}
