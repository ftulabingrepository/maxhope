@extends('layouts.app')

@section('content')
<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>Level</div>
      </h1>
      <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="fa fa-list"></i> List</h4>
                </div>
                <div class="card-body">
                	<table class="table table-bordered" id="table-level">
						<thead>
							<tr>
								<th>Id</th>
								<th>Tier Level</th>
								<th>Slot</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($level as $l)
								<tr>
									<td>{{ $l->id }}</td>
									<td>{{ $l->tierlevel }}</td>
									<td>{{ $l->slot }}</td>
									<td><button class="btn btn-sm btn-primary l" data-id="{{ $l->id }}" data-toggle="modal" data-target="#levelmodal"><i class="fa fa-edit"></i> Edit</button></td>
								</tr>
							@endforeach
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
      </section>
 </div>
 <!--modal-->
 <div  id="levelmodal"class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Level</h5>
        <button type="button" class="close" onclick="$('#levelmodal').modal('hide');" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="updatelevel">
        @csrf
        <input type="hidden" name="levelid">
      <div class="modal-body">
      	<div class="form-group">
      		<label>Tier Level</label>
      		<input type="number" class="form-control" name="_tierlevel">
      	</div>
      	<div class="form-group">
      		<label>Slot</label>
      		<input type="number" class="form-control" name="_slot">
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="$('#levelmodal').modal('hide');">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('customjs')
<script src="{{asset('js/maintenance/level.js')}}"></script>
@endsection