@extends('layouts.app')


@section('content')

<div class="main-content">
<section class="section">
  <h1 class="section-header">
    <div>Monthly Profit Share - {{$mps->Month}} {{$mps->year}}</div>
  </h1>

  <div class="card">
    <div class="card-header">
    <h4><i class="ion ion-card"></i> Months</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session()->get('message') }}
            </div>
            @endif
            <form method="POST" action="submit" style="margin-bottom: 30px;">
              @csrf
              <div class="row">
                <div class="col-12 col-md-6">
                  <div class="form-group">
                    <label>Month</label>
                    <input type="text" name="" class="form-control" readonly value="{{$mps->Month}}">
                  </div>
                  <div class="form-group">
                    <label>Year</label>
                    <input type="text" name="" class="form-control" readonly value="{{$mps->year}}">
                  </div>
                  <div class="form-group">
                    <label>Distributed</label>
                    <input type="text" name="" class="form-control" readonly value="{{$mps->distributed}}">
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="form-group">
                    <label>Qualified</label>
                    <input type="text" name="" class="form-control" readonly value="{{$mps->member_count}}">
                  </div>
                  <div class="form-group">
                    <label>Amount</label>
                    <input type="text" name="" class="form-control" readonly value="{{$mps->amount}}">
                  </div>
                  <div class="form-group">
                    @if($mps->distributed == 'NO')
                    <button class="btn btn-primary float-right">Distribute</button>
                    @endif
                  </div>
                </div>
              </div>
            </form>
            <div style="padding: 0 15px; overflow-x: auto;" class="tbl-datatables">
                    <table class="table table-bordered" id="tbl-qualify" style="border-collapse: collapse!important;">
                      <thead>
                          <tr>
                              <th>Username</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                          </tr>
                      </thead>
                    </table>
                </div>
        </div>
        <div class="col-12 col-md-6">
          <form></form> 
        </div>
    </div>

  </div>
  </div>

</section>
</div>

@endsection

@section('customjs')
<script src="{{asset('js/mps/mps.js')}}"></script>
@endsection