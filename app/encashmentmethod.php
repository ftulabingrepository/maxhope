<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class encashmentmethod extends Model
{
    protected $table = 'encashmentmethods'; 
    protected $fillable = [
        'name'
    ];
}
