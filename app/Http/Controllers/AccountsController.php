<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, DB, Session;
use App\User;
use Yajra\DataTables\Facades\DataTables;

class AccountsController extends Controller
{

    public function index(){
        $userid = Auth::user()->id;
        $usertype = Auth::user()->type;
        if($usertype == 2){
            $ownerRec = false;
            $accounts = DB::table('sys_accounts as s')
                        ->select('u.id', 'u.username', 'u.firstname', 'u.lastname', 'u.middlename', 'u.id', 'u.created_at')
                        ->leftJoin('users as u', 'u.id', '=', 's.userid')
                        ->where('owner', $userid)->get();
            $checksys = DB::table('sys_accounts')->where('userid', $userid)->first();
             // dd($accounts);
            if($accounts->count() > 0){

                $ownerRec = DB::table('users')->where('id', $userid)->first();
                if (!is_null($checksys)) {
                    # code...
                }
                // dd($accOwner);
                // $owner = $accOwner->owner;

                // $ownerRec = User::find($owner);

                // $accounts = DB::table('sys_accounts as s')
                //         ->select('u.id', 'u.username', 'u.firstname', 'u.lastname', 'u.middlename', 'u.id', 'u.created_at')
                //         ->leftJoin('users as u', 'u.id', '=', 's.userid')
                //         ->where('userid', '<>', $userid)
                //         ->where('owner', $owner)->get();

            }else{
                if(!is_null($checksys)){
                   $accounts = DB::table('sys_accounts as s')
                            ->select('u.id', 'u.username', 'u.firstname', 'u.lastname', 'u.middlename', 'u.id', 'u.created_at')
                            ->leftJoin('users as u', 'u.id', '=', 's.userid')
                            ->where('owner', $checksys->owner)->get();
                    $ownerRec = DB::table('users')->where('id', $checksys->owner)->first();
                }else{
                    $ownerRec = DB::table('users')->where('id', $userid)->first();
                }
            }

            return view('accounts', compact('accounts', 'ownerRec'));
        }else{
            return view('adminaccounts');
                }
        

    }

    public function logoutAcc(Request $request){
    	$user = User::find($request->btnID);

	    	Auth::login($user, true);
	    	return redirect('dashboard');
    	//return redirect('login')->with('request');
    }
    public function getAccounts(){
        $users = DB::table('users as u')->select(DB::raw('DATE_FORMAT("u.created_at", "%d/%l/%Y")'), 'u.created_at', 'u.username', 'u.firstname', 'u.middlename', 'u.lastname', 'u.id')->where('type', 2);
        // dd($users);
        $token = csrf_token();
            return Datatables::of($users)
                    ->setRowId(function ($users) {
                        return $users->id;
                    })
                    ->addColumn('action', function ($users) use ($token) {

                            return '<a class="btn btn-success btn-sm" href="/maintenance/user/'.$users->id.'/details"><i class="fa fa-edit"></i></a><form style="display: inline-block; margin-left: 3px;" method="POST" action="logoutAcc">
                                <input value="'.$token.'" name="_token" type="hidden">
                                <button name="btnID" value="'.$users->id.'" class="btn btn-sm btn-primary"> <i class="fa fa-eye"></></button></form>';

                    })
                    ->make(true);
    }
}
