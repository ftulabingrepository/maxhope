<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userFunds extends Model
{
     protected $table = 'userfunds';
    protected $fillable = [
        'userid', 'registrationFunds', 'payoutFunds'
    ];

}
