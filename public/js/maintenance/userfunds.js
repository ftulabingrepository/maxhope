$(document).ready(function(){
	$('input[list]').on('input', function(e) {
	    var $input = $(e.target),
	        $options = $('#' + $input.attr('list') + ' option'),
	        $hiddenInput = $('#' + $input.attr('id') + '-hidden'),
	        label = $input.val();

	    $hiddenInput.val(label);

	    for(var i = 0; i < $options.length; i++) {
	        var $option = $options.eq(i);

	        if($option.text() === label) {
	            $hiddenInput.val( $option.attr('data-value') );
	            break;
	        }
	    }
	});

});