  @extends('layouts.app')

@section('content')
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h6 class="modal-title">Create Account</h6>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ url('addAccount') }}">
              @csrf
        <div class="row">
          <div class="col-xs-6 col-lg-6" style="padding: 20px;">
            <label class="label" for="firstname">First Name</label>
              <input class="form-control" name="firstname" id="firstname" type="text" placeholder="First Name" required>
              <label class="label" for="middlename">Middle Name</label>
              <input class="form-control" name="middlename" id="middlename" type="text" placeholder="Middle Name" required>
              <label class="label" for="lastname">Last Name</label>
              <input class="form-control" name="lastname" id="lastname" type="text" placeholder="Last Name" required>
              <label class="label" for="mobileno">Mobile Number</label>
              <input class="form-control" name="mobileno" id="mobileno" type="text" placeholder="Mobile Number" required>
               
          </div>
          <div class="col-xs-6 col-lg-6" style="padding: 20px;">
               <label class="label" for="zipcode">Zip Code</label>
              <input class="form-control" name="zipcode" id="zipcode" type="text" placeholder="Zip Code" required value="093">
              <label class="label" for="email">Active Email Address</label>
              <input class="form-control" name="email" id="email" type="text" placeholder="Email" required>
              <label class="label" for="newUsername">Username</label>
              <input class="form-control" name="newUsername" id="newUsername" type="text" placeholder="Username" required>
              <label class="label" for="password">Password</label>
              <input class="form-control" type="password" name="password" id="password" type="text" placeholder="Password" required value="123123">
          </div>
          <div class="col-xs-12">
            <label class="label" for="address1">Address</label>
              <input class="form-control" name="address1" id="address1" type="text" placeholder="Address" required>
          </div>
        </div>
        <div class="clearfix">
            <div class="col-xs-6">
                <button class="btn btn-primary btn-block" style="margin-top: 10px;" id="addAccountBtn">Add Account</button>
            </div>
        </div>
      </form>
      </div>
    <!--   <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>

<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>Dashboard</div>
      </h1>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
              <a href="{{route('earningssummary')}}">
                <div class="card card-sm-3">
                  <div class="card-icon bg-primary">
                    <i class="fa fa-dollar-sign"></i>
                  </div>
                  <div class="card-wrap">
                    <div class="card-header">
                      <h4>Accumulated Earnings</h4>
                    </div>
                    <div class="card-body">
                      @if($accumulated > 0)
                        &#8369;{{number_format($accumulated, 2)}}
                      @else
                        &#8369;{{number_format($accumulated, 2)}}
                      @endif
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
              <div class="card card-sm-3">
                <div class="card-icon bg-danger">
                  <i class="fa fa-user-plus"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Registration Funds</h4>
                  </div>
                  <div class="card-body">
                    &#8369;{{ number_format($regFunds, 2)}}
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
              <div class="card card-sm-3">
                <div class="card-icon bg-mhope">
                  <i class="fa fa-dollar-sign"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>i - Advance Earnings</h4>
                  </div>
                  <div class="card-body">
                    &#8369;{{number_format($payoutFunds, 2)}}
                  </div>
                  
                    <a href="#" class="" data-toggle="modal" data-target="#encashmentModal">
                     <i class="ion ion-card"></i>
                    </a>
                 
                  
                </div>
              </div>
            </div>         
          </div>
        <!--accounts area-->
        <div class="row" style="display: {{$hide}}">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="fa fa-user"></i> Accounts</h4>
                </div>
                <div class="card-body">
                  <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal">ADD ACCOUNT</button>
                  @if ($errors->any())
                    <div class="alert alert-danger dash-danger" style="margin-top: 10px;">
                        <ul class="text-center">
                                <div class="alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Error!</strong>  {{ $errors->first() }}
                                </div>
                        </ul>
                    </div>
                @endif
                </div>
            </div>
          </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="fa fa-table"></i> Tables</h4>
                </div>
               <!--  <div class="clearfix" >
                  <div class="col-md-offset-8 col-md-4" style="float: right">
                    <select class="form-control">
                      <option disabled selected>Select account</option>
                      @if($myAccounts)
                        @foreach($myAccounts as $account)
                          <option>{{$account->username}}</option>
                        @endforeach
                      @endif 
                    </select>
                  </div>
                </div> -->
                <div class="card-body">

                  @if($movements)
                        <ul class="nav nav-tabs itbl-tabs" style="display:block; text-align:center;">
                            @for($y = 1; $y <= 6; $y++)
                           <li class="nav-item @if($currentTable == $y)
                                @endif" style="display: inline-block;"><a class="nav-link @if($currentTable == $y) active
                                @endif" href="#tab{{$y}}" data-toggle="tab">iTable {{$y}}</a></li>
                           @endfor
                        </ul>
                            <div class="tab-content">
                            @for($z = 1; $z <= 6; $z++)
                                
                                <div class="itable table{{$z}} @if($currentTable == $z) active @endif tab-pane" id="tab{{$z}}">
                                    <div class="row">
                                    
                                    @for($x = 1; $x <= 13; $x++)

                                        @if($x == 1)
                                        <div class="col-12 list-group-item tbl-slot">
                                        @elseif($x <= 3)
                                        <div class="col-12 col-md-6 list-group-item tbl-slot">
                                        @elseif($x <= 7)
                                        <div class="col-12 col-md-3 list-group-item tbl-slot">
                                        @else
                                        <div class="col-12 col-md-2 list-group-item tbl-slot">
                                        @endif
                                            <div class="tbl-slot-in">
                                                <a class="slot-name">
                                                    @foreach($movements[$z] as $movement)
                                                        @if($x==$movement->movedto)
                                                            @if($movement->userid != 0)
                                                                <a href="#" data-toggle="tooltip" title="{{$movement->firstname}} {{$movement->lastname}} ({{$movement->mobileno}})">{{$movement->username}}</a>
                                                            @else
                                                                OPEN
                                                            @endif
                                                        @endif 
                                                    @endforeach
                                                </a>
                                                <span class="slot-num">slot {{$x}}</span>
                                            </div>
                                        </div>
                                      
                                       
                                    @endfor
                                    @if($z == 1)
                                    <p class="d-block" style="margin-top: 20px; font-size: 26px;"><i class="ion ion-ios-circle-filled ion1"></i> iAdvance Perks &#8369;4,000.00</p>
                                    @elseif($z == 2)
                                    <p class="d-block" style="margin-top: 20px; font-size: 26px;"><i class="ion ion-ios-circle-filled ion1"></i> iAdvance Perks &#8369;11,000.00</p>
                                    @elseif($z == 3)
                                    <p class="d-block" style="margin-top: 20px; font-size: 26px;"><i class="ion ion-ios-circle-filled ion1"></i> iAdvance Perks &#8369;40,000.00</p>
                                    @elseif($z == 4)
                                    <p class="d-block" style="margin-top: 20px; font-size: 26px;"><i class="ion ion-ios-circle-filled ion1"></i> iAdvance Perks &#8369;120,000.00</p>
                                    @elseif($z == 5)
                                    <p class="d-block" style="margin-top: 20px; font-size: 26px;"><i class="ion ion-ios-circle-filled ion1"></i> iAdvance Perks &#8369;600,000.00</p>
                                    @elseif($z == 6)
                                    <p class="d-block" style="margin-top: 20px; font-size: 26px;"><i class="ion ion-ios-circle-filled ion1"></i> iAdvance Perks &#8369;1,000,000.00</p>
                                    @endif
                                    <p class="d-block" style="font-size: 26px;"><i class="ion ion-ios-circle-filled ion2"></i> Free 3 Accounts Re-Entry in iTable{{$z}}</p>
                                    @if($z < 6)
                                    <p class="d-block" style="font-size: 26px;"><i class="ion ion-ios-circle-filled ion3"></i> Free Account Entry in iTable{{$z + 1}}</p>
                                    @endif
                                    @if($z == 4)
                                    <p class="d-block" style="font-size: 26px;"><i class="ion ion-ios-circle-filled ion3"></i> Travel Incentives</p>
                                    @endif
                                    @if($z >= 5)
                                    <p class="d-block" style="font-size: 26px;"><i class="ion ion-ios-circle-filled ion3"></i> Car Down Payment</p>
                                    @endif
                                </div>
                                </div>

                            @endfor

                        </div>
                        </div>
                        
                    @else
                        <div class="row">
                            <div class="col-xs-12">
                                <a href="{{ url('createGroup') }}"><button class="btn btn-lg btn-block btn-primary">Create Group</button></a>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
              </div>
            </div>
        </div>
    </section>
</div>
<!--end tables area-->
<!--modals-->
<div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" onclick="$('#myModal').modal('hide')">&times;</button>
          </div>
          <div class="modal-body">
            <h3>Parent Table here...</h3>
          </div>
        </div>

      </div>
    </div>
<!--end modals-->
<div  id="encashmentModal"class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Request Earnings</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="payout-tab" data-toggle="tab" href="#payout" role="tab" aria-controls="payout" aria-selected="true">Payout</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" id="profile-tab" data-toggle="tab" href="#convert" role="tab" aria-controls="convert" aria-selected="false">Convert To Registration Fund</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="payout" role="tabpanel" aria-labelledby="payout-tab">
            <form method="POST" action="" id="formEncashment">
               @csrf
               
              <div class="form-group">
                <select class="form-control" name="name" required id="encashmentMethod">
                    <option value="" selected>Select Encashment Method</option>
                  @foreach($encashmentmethods as $encashmentmethod)
                    <option value="{{$encashmentmethod->id}}" encashtype="{{$encashmentmethod->type}}">{{$encashmentmethod->name}}</option>
                  @endforeach
                </select>
               <!--  <p class="note"><i class="fa fa-note"></i>Note: Please ensure that all encashment information is correct.</p> -->
              </div>
              <div class="form-group">  

                <label>Available Earnings &#8369;({{number_format($payoutFunds, 2)}})</label>
                <input class="form-control" type="text" name="amount" placeholder="" id="encashmentAmount" required>
                <p class="note"> VERY IMPORTANT: Ensure to accurately fill out and update both the bank account and remittance receiver's details prior to your request. Negative (-) and Zero (0) figure is not allowed. Minimum encashment after exit from table 1 is &#8369;500.00.</p>
              </div>
              <input type="hidden" name="recipient" id="recipientValue" value="1">
              @if(!is_null($recipientAvailable))
               <div class="form-group" id="recipientCont" style="display: none;">
                 <!-- <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="customCheck1" value="1" required>
                  <label class="custom-control-label" for="customCheck1">Recipient</label>
                  
                </div> -->
               </div>
               @endif
              <p class="message" id=encashmentMessage></p>
              
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Pay Out</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                
              </div>
            </form>
          </div>
          <div class="tab-pane fade" id="convert" role="tabpanel" aria-labelledby="convert-tab">
            <form method="POST" action="" id="formConvert">
               @csrf
              
              <div class="form-group"> 
                <p class="note">Note: You will be charged with 6% Admin Fee.</p> 
                <label>Available Earnings &#8369;({{number_format($payoutFunds, 2)}})</label>
                <input class="form-control" type="text" name="amount" placeholder="" id="encashmentAmountConvert" required>
              </div>
              <p class="note">Note: Negative (-) and Zero (0) figure is not allowed.</p>
              <p class="message" id=encashmentMessage></p>
              
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Convert</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                
              </div>
            </form>
          </div>
          
        </div>
          
      </div>
      
      
    </div>
  </div>
</div>
@endsection
@section('customjs')
<script src="{{asset('js/dashboard/encashment.js')}}"></script>
@endsection