@extends('layouts.app')

@section('content')

<div class="main-content earning-summary-page">
    <section class="section">
      <h1 class="section-header">
        <div>Pay Out</div>
      </h1>
       
        <!--accounts area-->
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    
                    <div class="card-body">
                        @if(session()->has('message'))
                          <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('message') }}
                          </div>
                          @endif
                        <form class="form-serg f-edit" method="POST" action="submit">
                            @csrf
                            
                            <input type="hidden" name="id" value="{{$en->id}}">
                             <input type="hidden" name="email" value="{{$en->email}}">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <h3 class="primary-color">Encashment Information</h3>
                                    <div class="form-group">
                                        <label for="username">User Name</label>
                                        <input type="text" class="form-control" id="username" value="{{$en->username}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="firstname">First Name</label>
                                        <input type="text" class="form-control" name="firstname" value="{{$en->firstname}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="lastname">Last Name</label>
                                        <input type="text" class="form-control" name="lastname" value="{{$en->lastname}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="lastname">Address</label>
                                        <input type="text" class="form-control" name="address" value="{{$en->address}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="lastname">Mobile Number</label>
                                        <input type="text" class="form-control" name="mobileno" value="{{$en->mobileno}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="encashmentmode">Encashment Mode</label>
                                        <input type="text" class="form-control" name="encashmentmode" value="{{$en->encashmentname}}" readonly>
                                    </div>
                                    @if($en->etid == 1)
                                    <div class="form-group">
                                        <label for="encashmentmode">Bank Account Name</label>
                                        <input type="text" class="form-control" name="bankaccountname" value="{{$en->bankname}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="encashmentmode">Bank Account Number</label>
                                        <input type="text" class="form-control" name="bankaccountnumber" value="{{$en->banknumber}}" readonly>
                                    </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="date">Date</label>
                                        <input type="text" class="form-control" name="date" value="{{$en->created_at}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <input type="text" class="form-control" name="status" value="{{$en->status}}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <h3 class="primary-color">Encashment Amount</h3>
                                    <div class="form-group">
                                        <label for="encashmentamount">Amount</label>
                                        <input type="text" class="form-control" name="encashmentamount" value="&#8369;{{number_format($en->amount, 2)}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="netamount">Net Amount</label>
                                        <input type="text" class="form-control" name="netamount" value="&#8369;{{number_format($en->net,2)}}" readonly id="netAmountDisplay">
                                        <input type="hidden" id="netAmount" value="{{number_format($en->net,2)}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tax">Withholding Tax</label>
                                        <input type="text" class="form-control" name="tax" value="&#8369;{{number_format($en->tax,2)}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="adminfee">Admin Fee</label>
                                        <input type="text" class="form-control" name="adminfee" value="&#8369;{{number_format($en->adminfee,2)}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        @if($en->methodid == 2)
                                        <label for="othercharges">Other Charges</label>
                                            <input type="number" class="form-control" name="othercharges" placeholder="Charge(1000)" id="otherCharges" readonly>
                                        @else
                                            @if($en->statusid == 2)
                                            <label for="othercharges">Other Charges</label>
                                        <input type="number" class="form-control"  value="{{number_format($en->othercharges,2)}}" readonly>
                                        <label for="othercharges">Reference Number</label>
                                        <input type="text" class="form-control" name="reference" required placeholder="Reference Number"  readonly value="{{$en->reference_number}}">
                                            @else
                                                <label for="othercharges">Other Charges</label>
                                              <input type="number" class="form-control" name="othercharges" required placeholder="Charge(1000)" id="otherCharges" >
                                              <label for="othercharges">Reference Number</label>
                                              <input type="text" class="form-control" name="reference" required placeholder="Reference Number"  >
                                            @endif
                                            
                                        @endif
                                        
                                    </div>
                                    @if($en->recipient == 1)
                                        <h3 class="primary-color">Remittance Reciever's Details</h3>
                                        <div class="form-group">
                                            <label for="encashmentamount">Complete Name</label>
                                            <input type="text" class="form-control" value="{{$en->repfirstname}} {{$en->replastname}}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="encashmentamount">Address</label>
                                            <input type="text" class="form-control" value="{{$en->repaddress}}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="encashmentamount">Mobile Number</label>
                                            <input type="text" class="form-control" value="{{$en->repmobileno}}" readonly>
                                        </div>
                                    @endif
                                    
                                </div>
                                
                            </div>
                            @if($en->statusid == 1)
                                <div class="f-btn-cont">
                                <button class="btn btn-info" type="button" id="compute">Compute</button>
                                <button class="btn btn-primary">Approve</button>
                            </div>
                            @endif
                            
                            
                        </form>
                        

                    </div>
                </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
    <!-- <div style="padding: 0 15px;" class="tbl-datatables">
        <table class="table table-bordered" id="tbl-payout">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                     <th>First Name</th>
                                    <th>Last Name</th> 
                                    <th>Amount</th>
                                    <th>Net</th>
                                    <th>Encashment Method</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
    </div> -->
</div>

@endsection
@section('customjs')
<script src="{{asset('js/payout/payout.js')}}"></script>
@endsection