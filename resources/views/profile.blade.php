@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>Profile</div>
      </h1>
       
        <!--accounts area-->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="ion ion-android-person"></i> Personal Information</h4>
                </div>
                <div class="card-body">
                   
                    @if(session()->has('message'))
                      <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ session()->get('message') }}
                      </div>
                    @elseif(session()->has('error'))
                      <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong>  {{ session()->get('error') }}
                      </div>
                    @endif
                    @if($checksysadd == 0)
                        <form method="POST" action="">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="username">User Name</label>
                                        <input type="text" class="form-control" id="username" value="{{$user->username}}" readonly name="username">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">First Name</label>
                                        <input type="text" class="form-control" id="username" value="{{$user->firstname}}" readonly name="firstname">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Middle Name</label>
                                        <input type="text" class="form-control" id="username" value="{{$user->middlename}}" readonly name="middlename">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Last Name</label>
                                        <input type="text" class="form-control" id="username" value="{{$user->lastname}}" readonly name="lastname">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Suffix</label>
                                        <input type="text" class="form-control" id="username" value="{{$user->suffix}}" readonly name="suffix">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="username">Address</label>
                                        <textarea class="form-control" readonly name="address">{{$user->address}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Zipcode</label>
                                        <input type="text" class="form-control" id="username" value="{{$user->zipcode}}" readonly name="zipcode">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Mobile Number</label>
                                        <input type="text" class="form-control" id="username" value="{{$user->mobileno}}" readonly name="mobileno">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Email Address</label>
                                        <input type="text" class="form-control" id="username" value="{{$user->email}}" readonly name="">
                                    </div>
                                    
                                </div>
                            </div>
                        </form>
                    @else
                        <form method="POST" action="profile/edit">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <input type="hidden" name="id" value="{{$userid}}">
                                <div class="form-group">
                                    <label for="username">User Name</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->username}}" readonly name="username">
                                </div>
                                <div class="form-group">
                                    <label for="username">First Name</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->firstname}}"  name="firstname">
                                </div>
                                <div class="form-group">
                                    <label for="username">Middle Name</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->middlename}}"  name="middlename">
                                </div>
                                <div class="form-group">
                                    <label for="username">Last Name</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->lastname}}"  name="lastname">
                                </div>
                                <div class="form-group">
                                    <label for="username">Suffix</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->suffix}}"  name="suffix">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="username">Address</label>
                                    <textarea class="form-control"  name="address">{{$user->address}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="username">Zipcode</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->zipcode}}"  name="zipcode">
                                </div>
                                <div class="form-group">
                                    <label for="username">Mobile Number</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->mobileno}}"  name="mobileno">
                                </div>
                                <div class="form-group">
                                    <label for="username">Email Address</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->email}}"  name="email">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary float-right">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endif
                    <div class="row">
                    <div class="col-12 col-md-6">
                        <h3 class="primary-color">Bank Account Details</h3>
                        <form method="POST" action="{{ url('profile/update') }}">
                        @csrf
                            @if(is_null($user->name) || is_null($user->number))
                            <div class="form-group">
                                        <label for="accountname">Bank Name</label>
                                        <input type="text" class="form-control" name="bankname" value="{{$user->bankname}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="accountname">Bank Account Name</label>
                                        <input type="text" class="form-control" name="accountname" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="accountnumber">Bank Account Number</label>
                                        <input type="number" class="form-control" name="accountnumber" required>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label for="accountname">Bank Name</label>
                                        <input type="text" class="form-control" name="bankname" value="{{$user->bankname}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="accountname">Bank Account Name</label>
                                        <input type="text" class="form-control" name="accountname" value="{{$user->name}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="accountname">Bank Account Number</label>
                                        <input type="number" class="form-control" name="accountnumber" value="{{$user->number}}" required>
                                    </div>
                                @endif
                                <p class="note">Very Important: Ensure to accurately fill out and update both the bank account and remittance receiver's details prior to your encashment request.</p>
                                <button type="submit" class="btn btn-primary f-right">Update</button>

                        </form>
                    </div>
                    <div class="col-12 col-md-6">
                        <h3 class="primary-color">Remittance Recipient Details</h3>
                        @if(is_null($recipient))
                            <form method="POST" action="{{ url('profile/recipient') }}">
                            @csrf
                                <div class="form-group">
                                    <label for="firstname">First Name</label>
                                    <input type="text" name="firstname" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="lastname">Last Name</label>
                                    <input type="text" name="lastname" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="middlename">Middle Name</label>
                                    <input type="text" name="middlename" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input type="text" name="address" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="mobileno">Mobile Number</label>
                                    <input type="text" name="mobileno" class="form-control" required>
                                </div>
                                <button type="submit" class="btn btn-primary f-right">Submit</button>
                            </form>
                        @else
                            <form method="POST" action="{{ url('profile/recipient/edit') }}">
                            @csrf
                                <div class="form-group">
                                    <label for="firstname">First Name</label>
                                    <input type="text" name="firstname" class="form-control" required value="{{$recipient->firstname}}">
                                </div>
                                <div class="form-group">
                                    <label for="lastname">Last Name</label>
                                    <input type="text" name="lastname" class="form-control" required value="{{$recipient->lastname}}">
                                </div>
                                <div class="form-group">
                                    <label for="middlename">Middle Name</label>
                                    <input type="text" name="middlename" class="form-control" required value="{{$recipient->middlename}}">
                                </div>
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input type="text" name="address" class="form-control" required value="{{$recipient->address}}">
                                </div>
                                <div class="form-group">
                                    <label for="mobileno">Mobile Number</label>
                                    <input type="text" name="mobileno" class="form-control" required value="{{$recipient->mobileno}}">
                                </div>
                                <button type="submit" class="btn btn-primary f-right">Submit</button>
                            </form>
                        @endif
                        
                    </div>
                </div>
                </div>
              </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
</div>

@endsection
@section('customjs')
<!-- <script src="{{asset('js/maintenance/funds.js')}}"></script> -->
@endsection