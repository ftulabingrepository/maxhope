<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaction_methods extends Model
{
    protected $table = 'transaction_methods'; 
    protected $fillable = [
        'method'
    ];
}
