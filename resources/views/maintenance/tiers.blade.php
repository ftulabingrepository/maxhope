@extends('layouts.app')

@section('content')
<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>Tiers</div>
      </h1>
      <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="fa fa-list"></i> List</h4>
                </div>
                <div class="card-body">
                	<table class="table table-bordered" id="table-tiers">
						<thead>
							<tr>
								<th>Tier Name</th>
								<th>Level</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($tiers as $t)
								<tr>
									<td style="text-transform: capitalize;">{{$t->tiername}}</td>
									<td style="text-transform: capitalize;">{{$t->levels}}</td>
									<td><button class="btn btn-sm btn-primary te" data-id="{{ $t->id }}"><i class="fa fa-edit"></i> Edit</button></td>
								</tr>
							@endforeach
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
      </section>
 </div>
 <!--modals-->
 <div  id="tiersmodal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tiers</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="$('#tiersmodal').modal('hide')" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="updatetiers">
        @csrf
        <input type="hidden" name="tierid">
      <div class="modal-body">
        <div class="form-group">
        	<label>Tier Name</label>
        	<input type="text" name="tiername" class="form-control">
        </div>
        <div class="form-group">
        	<label>Tier Level</label>
        	<input type="text" name="levels" class="form-control">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="$('#tiersmodal').modal('hide')">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('customjs')
<script src="{{asset('js/maintenance/tier.js')}}"></script>
@endsection