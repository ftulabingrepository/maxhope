@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>Earning Types</div>
      </h1>
          <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4><i class="fa fa-list"></i> List</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form method="POST" action="{{ url('addGroup') }}">
                                        @csrf
                                        <input type="text" name="groupname" class="form-control btn-lg" placeholder="Group name" style="margin-bottom: 15px;" required>
                                        
                                </div>
                             <!--    <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-offset-2 col-md-4">
                                            <input type="text" class="form-control" name="firstname" placeholder="First Name" required>
                                            <input type="text" class="form-control" name="lastname" placeholder="Last Name" required>
                                            <input type="text" class="form-control" name="middlename" placeholder="Middle Name" required>
                                            <input type="text" class="form-control" name="mobile" placeholder="Mobile Number" required>
                                             <input type="text" class="form-control" name="address" placeholder="Address" required>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="zipcode" placeholder="Zip Code" required>
                                            <input type="text" class="form-control" name="email" placeholder="Email Address" required>
                                            <input type="text" class="form-control" name="username" placeholder="Username" required>
                                            <input type="text" class="form-control" name="password" placeholder="Password" required>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-md-12" style="margin-top: 10px;">
                                    <button class="btn btn-lg btn-primary btn-block">CREATE GROUP</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>

<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Group</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="POST" action="{{ url('addGroup') }}">
                                @csrf
                                <input type="text" name="groupname" class="form-control btn-lg" placeholder="Group name" style="margin-bottom: 15px;">
                                <button class="btn btn-lg btn-primary btn-block">CREATE GROUP</button>
                            </form>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div> -->

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <h3>Parent Table here...</h3>
          </div>
        </div>

      </div>
    </div>
</div>
<!-- <script type="text/javascript" src="{{ asset('js/usercontrol/addAccount.js')}}"></script> -->
@endsection
