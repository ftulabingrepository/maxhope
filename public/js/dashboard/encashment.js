$(document).ready(function(){

	//for testing...
	$("#newUsername").change(function(){
		var val = $("#newUsername").val();
		$("#firstname").val(val);
		$("#lastname").val(val);
		$("#middlename").val(val);
		$("#mobileno").val(val);
		$("#address1").val(val);
		$("#email").val(val);
	})
	// //.....end 
	// $('#customCheck1').change(function(){
	// 	if($(this).prop('checked') == true){
	// 	    $('#recipientValue').val(1);
	// 	}else{

	// 		$('#recipientValue').val(0);
	// 	}
	// });
	$('#formEncashment').submit(function(e){
		e.preventDefault();
		
		if (this.checkValidity()) {
				$('#formEncashment button[type=submit]').attr('disabled', 'disabled');
			var recipient = $('#customCheck1').val();
			var encashmentMethod = $('#encashmentMethod').val();
			var amounts = $('#encashmentAmount').val();
			var amount = parseFloat(amounts).toFixed(2);
			var data = $(this).serialize();
			
			// alert(encashmentMethod);
			// alert(amounts);
	    	$.ajax({
				type:'get',
		        url:'encashment',
		        // dataType:'json',
		        // data: "method="+encashmentMethod+"amount="+amount,
		        data: data,
		        success:function(data){
		        	$("#formEncashment")[0].reset()
		        	$("#formEncashment #recipientValue").val(0);
		        	alert(data);
		        	$('#formEncashment button[type=submit]').removeAttr('disabled');
		        	// $('#encashmentMessage').html(data);
		        },
		        error:function(data){
		        	alert(data);
		        	$('#formEncashment button[type=submit]').removeAttr('disabled');
		        	$("#formEncashment #recipientValue").val(0);
		        }
			});
		}
		
	});
	$('#formConvert').submit(function(e){
		e.preventDefault();
		if (this.checkValidity()) {
			var data = $(this).serialize();
	    	$.ajax({
				type:'POST',
		        url:'convert',
		        // dataType:'json',
		        // data: "method="+encashmentMethod+"amount="+amount,
		        data: data,
		        success:function(data){
		        	$("#formConvert")[0].reset()
		        	alert(data);
		        	// $('#encashmentMessage').html(data);
		        },
		        error:function(data){
		        	alert(data);
		        }
			});
		}
		
	});
	$('#encashmentModal').on('hidden.bs.modal', function () {
	 location.reload();
	})
	// $('#encashmentMethod').change(function(){
	// 	var method = $('option:selected', this).attr('encashtype')
	// 	if (method == 2) {
	// 		$('#recipientCont').css('display', 'block');
	// 	}else{
	// 		$('#recipientCont').css('display', 'none');
	// 	}
	// });
                   
});