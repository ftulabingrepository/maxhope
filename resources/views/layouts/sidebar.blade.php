<div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="{{route('home')}}">MaxHOPE</a>
          </div>
          <div class="sidebar-user">
            <div class="sidebar-user-picture">
            
              {{substr(Auth::user()->firstname,0,1)}}{{substr(Auth::user()->lastname,0,1)}}

            </div>
            <div class="sidebar-user-details">
              <div class="user-name">{{Auth::user()->firstname}} {{Auth::user()->lastname}} </div>
              <div class="user-role" style="font-size: 16px;">
                @if(Auth::user()->type == 2)
                 Affiliate
                @else
                 Administrator
                 
                @endif
                
              </div>
            </div>
          </div>
          <ul class="sidebar-menu">
            <!-- <li class="menu-header">Dashboard</li> -->
            @foreach($modules as $mod)
              @if($mod->parent == 0 && $mod->has_sub == 0)
                <li>
                <a href="{{ $mod->url }}"><i class="{{ $mod->icon }}"></i><span>{{ $mod->name }}</span></a>
                </li> 
              @elseif($mod->has_sub > 0 || $mod->parent == 0)
                <li>
                  <a href="#" class="has-dropdown"><i class="ion ion-ios-albums-outline"></i><span>{{ $mod->name }}</span></a>
                  <ul class="menu-dropdown">
                    @foreach($modules as $submod)
                      @if($submod->parent == $mod->mid)
                       <li><a href="<?php echo $submod->url; ?>"><i class="{{ $submod->icon }}"></i> {{ $submod->name }}</a></li>
                      @endif
                    @endforeach  
                  </ul>
                </li> 
                
              @endif
            @endforeach
            <li>
                  <a href="#" class="has-dropdown"><i class="ion ion-ios-albums-outline"></i><span>Products/Services</span></a>
                  <ul class="menu-dropdown">
                     <li><a href="#"><i class="ion ion-ios-circle-outline"></i>iAdvance</a></li>
                      <li><a href="#"><i class="ion ion-ios-circle-outline"></i>eHOPE Mall</a></li>
                      <li><a href="#"><i class="ion ion-ios-circle-outline"></i>eCourier</a></li>
                      <li><a href="#"><i class="ion ion-ios-circle-outline"></i>iBidding</a></li>
                      <li><a href="#"><i class="ion ion-ios-circle-outline"></i>iPaySolutions</a></li>
                      <li><a href="#"><i class="ion ion-ios-circle-outline"></i>eLoading</a></li>
                      <li><a href="#"><i class="ion ion-ios-circle-outline"></i>Trading</a></li>
                  </ul>
                </li> 
            
                
          </ul>
        </aside>
      </div>