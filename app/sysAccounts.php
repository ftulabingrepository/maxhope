<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sysAccounts extends Model
{
    protected $table = 'tiergroups'; 
    protected $fillable = [
        'userid', 'owner'
    ];
}
