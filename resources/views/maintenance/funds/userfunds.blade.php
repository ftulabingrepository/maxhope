@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>User Funds</div>
      </h1>
       
        <!--accounts area-->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card" style="min-height: 350px;">
                <div class="card-header">
                    <h4><i class="ion ion-card"></i> User Funds</h4>
                </div>
                <div class="card-body">
                    @if(session()->has('message'))
                      <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                         {{ session()->get('message') }}
                      </div>
                    @elseif(session()->has('error'))
                      <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong>  {{ session()->get('error') }}
                      </div>
                    @endif
                    <form method="get" action="{{ url('maintenance/user/funds/redirect') }}">
                        @csrf
                        <label class="label" for="useraccount">Account/Username</label>
                        <div class="row">

                            <div class="col-6">
                                
                                <input list="users" name="useraccount" style="width: 450px; max-width: 100%;" class="form-control" placeholder="Select Username" id="answer" required>
                                <datalist id="users">

                                    @foreach($userslist as $user)
                                        <option data-value="{{$user->id}}">{{$user->username}}</option>
                                    @endforeach
                                </datalist>
                                <input type="hidden" name="user" id="answer-hidden">
                            </div>
                            <div class="col-4">
                                <button class="btn btn-primary btn-block" type="submit">Amend Funds</button>
                            </div>
                        </div>                            
                       
                    </form>
                </div>
              </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
</div>

@endsection
@section('customjs')
<script src="{{asset('js/maintenance/userfunds.js')}}"></script
@endsection