<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class msps_details extends Model
{
    protected $table = 'msps_details';
    protected $fillable = [
        'msps_id', 'qualified'
    ];
}
