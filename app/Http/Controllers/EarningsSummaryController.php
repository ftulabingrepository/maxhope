<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class EarningsSummaryController extends Controller
{
    public function view(){
    	$userid = Auth::user()->id;
    	$earnings = DB::table('earnings as e')
    		->select(DB::raw('SUM(e.amount) as total'), 't.type', 't.id as tid')
    		->leftjoin('earning_types as t', 'e.earningTypeId', '=', 't.id')
    		->where('userid', $userid)
    		->groupby('e.earningTypeId', 't.type', 'tid')
    		->get();
		$accumulated = DB::table('earnings as e')
		->select(DB::raw('SUM(e.amount) as total'))
		->where('userid', $userid)
		->first();
        $exit = DB::table('earnings')->where('userid', $userid)->where('earningTypeId', 2)->first();
        $sysgen = DB::table('sys_accounts')->where('userid', $userid)->first()  ;
        // dd($sysgen);
		$total = $accumulated->total;
    	return view('earningssummary.index', compact('earnings', 'total', 'exit', 'sysgen'));
    }
}
