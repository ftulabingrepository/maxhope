<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\NotificationLog;
use App\Modules, App\ModulesAccess;

class SidebarServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
       view()->composer('layouts.app', function($view) {

            $user = Auth::user()->type;
            
            $modules = Modules::join('modules_accesses as ma', 'modules.id', '=', 'ma.module_id')->select(['modules.parent','modules.has_sub','modules.name','modules.url','modules.icon','modules.id as mid'])->where('grant_access', 1)->where('usertype', $user)->get();

             $notificationlog = NotificationLog::orderBy('id', 'DESC')->take(10)->get();

            $view->with('modules', $modules)->with('notificationlog', $notificationlog);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
