<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\encashments;
use DB;
use App\other_charges;
use App\admin_fee;
use App\userFunds;
use Auth;
use App\User;
use App\activity_logs;
use Yajra\DataTables\Facades\DataTables;
class PayOutController extends Controller
{
    public function view(){
        $usertype = Auth::user()->type;
        $total =  DB::table('encashments')->select(DB::raw('SUM(amount) as totalamount'), DB::raw('SUM(net) as totalnet'))->first();
        $adminfee = DB::table('admin_fees')->select(DB::raw('SUM(amount) as totaladminfee'))->first();
        $tax = DB::table('taxes')->select(DB::raw('SUM(amount) as totaltax'))->first();
        $charges = DB::table('other_charges')->select(DB::raw('SUM(amount) as totalcharges'))->first();
        return view('payout.index', compact('usertype', 'total', 'adminfee', 'tax', 'charges'));
    }
    public function getEncashments(){
        $userid = Auth::user()->id;
        $usertype = Auth::user()->type;
        
        if ($usertype == 2) {
            $en = DB::table('encashments as e')
            ->select('e.id', 'u.firstname', 'u.middlename', 'u.lastname', 'e.amount', 'e.net', 'em.name', 'es.status', 'tm.method', 'u.username', 'oc.amount as othercharges', 'af.amount as adminfee', 't.amount as tax', 'e.created_at')
            ->leftjoin('users as u', 'e.userid', '=', 'u.id')
            ->leftjoin('encashmentmethods as em', 'e.encashmentmethod', '=', 'em.id' )
            ->leftjoin('encashment_status as es', 'e.status', '=', 'es.id')
            ->leftjoin('transaction_methods as tm', 'e.transactionmethod', '=', 'tm.id')
            ->leftjoin('other_charges as oc', 'e.id', '=', 'oc.encashment_id')
            ->leftjoin('admin_fees as af', 'e.id', '=', 'af.encashment_id')
            ->leftjoin('taxes as t', 'e.id', '=', 't.encashment_id')
            ->where('e.userid', $userid)
            ->get();
           // dd($userid);
        return Datatables::of($en)
            ->setRowId(function ($en) {
                return $en->id;
            })
            ->editColumn('amount', function ($data) {
                $amount = number_format($data->amount, 2);
                return '₱'.$amount;
            })
            ->editColumn('net', function ($data) {
                $amount = number_format($data->net, 2);
                return '₱'.$amount;
            })
            ->editColumn('othercharges', function ($data) {
                $amount = number_format($data->othercharges, 2);
                return '₱'.$amount;
            })
            ->editColumn('adminfee', function ($data) {
                $amount = number_format($data->adminfee, 2);
                return '₱'.$amount;
            })
            ->editColumn('tax', function ($data) {
                $amount = number_format($data->tax, 2);
                return '₱'.$amount;
            })
            ->make(true);
        
        }elseif ($usertype == 1) {
            $en = DB::table('encashments as e')
            ->select('e.id', 'u.firstname', 'u.middlename', 'u.lastname', 'e.amount', 'e.net', 'em.name', 'es.status', 'tm.method', 'u.username', 'oc.amount as othercharges', 'af.amount as adminfee', 't.amount as tax', 'e.created_at')
            ->leftjoin('users as u', 'e.userid', '=', 'u.id')
            ->leftjoin('encashmentmethods as em', 'e.encashmentmethod', '=', 'em.id' )
            ->leftjoin('encashment_status as es', 'e.status', '=', 'es.id')
            ->leftjoin('transaction_methods as tm', 'e.transactionmethod', '=', 'tm.id')
            ->leftjoin('other_charges as oc', 'e.id', '=', 'oc.encashment_id')
            ->leftjoin('admin_fees as af', 'e.id', '=', 'af.encashment_id')
            ->leftjoin('taxes as t', 'e.id', '=', 't.encashment_id')
            ->get();
        return Datatables::of($en)
            ->setRowId(function ($en) {
                return $en->id;
            })
            ->addColumn('action', function ($en) {

                    return '<a href="maintenance/payout/'.$en->id.'/edit" class="btn btn-sm btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> View</a>';

            })
            ->editColumn('amount', function ($data) {
                $amount = number_format($data->amount, 2);
                return '₱'.$amount;
            })
            ->editColumn('net', function ($data) {
                $amount = number_format($data->net, 2);
                return '₱'.$amount;
            })
            ->editColumn('othercharges', function ($data) {
                $amount = number_format($data->othercharges, 2);
                return '₱'.$amount;
            })
            ->editColumn('adminfee', function ($data) {
                $amount = number_format($data->adminfee, 2);
                return '₱'.$amount;
            })
            ->editColumn('tax', function ($data) {
                $amount = number_format($data->tax, 2);
                return '₱'.$amount;
            })
            ->make(true);
        }
        
        
    }
    public function edit($id){
        $en = DB::table('encashments as e')
            ->select('e.id', 'u.firstname', 'u.lastname', 'e.amount', 'e.net', 'em.name', 'es.status', 'e.created_at', 't.amount as tax', 'af.amount as adminfee', 'e.status as statusid', 'em.name as encashmentname', 'tm.method', 'e.transactionmethod as methodid', 'u.username', 'oc.amount as othercharges', 'ba.name as bankname', 'ba.number as banknumber', 'et.id as etid', 'e.userid as heh', 'u.address', 'u.mobileno', 'u.email', 'rep.firstname as repfirstname', 'rep.lastname as replastname', 'rep.mobileno as repmobileno', 'rep.address as repaddress', 'e.recipient', 'e.reference_number')
            ->leftjoin('users as u', 'e.userid', '=', 'u.id')
            ->leftjoin('encashmentmethods as em', 'e.encashmentmethod', '=', 'em.id' )
            ->leftjoin('encashment_status as es', 'e.status', '=', 'es.id')
            ->leftjoin('taxes as t', 'e.id', '=', 't.encashment_id')
            ->leftjoin('admin_fees as af', 'e.id', '=', 'af.encashment_id')
            ->leftjoin('transaction_methods as tm', 'e.transactionmethod', '=', 'tm.id')
            ->leftjoin('other_charges as oc', 'e.id', '=', 'oc.encashment_id')
            ->leftjoin('bank_accounts as ba', 'u.id', '=', 'ba.userid')
            ->leftjoin('encashment_types as et', 'em.type', '=', 'et.id')
            ->leftjoin('recipients as rep', 'e.userid', '=', 'rep.userid')
            ->where('e.id', $id)
            ->first();
            // dd($en->heh);
        return view('payout.edit', compact('en'));
    }
    public function editSubmit(Request $request, $id){
        // $id = $request->id;
        $userid = Auth::user()->id;
        $encashment = encashments::find($id);
        $strnet = str_replace('₱', '', $request->netamount);
        // dd($strnet);
        // $nets = number_format($strnet,2);
        $nets = $strnet;
        $lastamount = str_replace(',', '', $nets);
        $net = (float)$lastamount;
        // dd($request->othercharges);
        $charges = $request->othercharges;
        $encash = encashments::find($id);
        $encash->net = $lastamount;
        $encash->reference_number = $request->reference;
        $encash->status = 2;
        $encash->save();
        $encashFrom = User::find($encash->userid);
        $description = $encashFrom->username.' encashment amount of '.$encashment->amount.' has been approved.';
        $this->activityLogs($description, $userid, $id, 1);
        if ($encashment->transactionmethod == 1) {
        $charge = new other_charges;
        $charge->amount = $charges; 
        $charge->userid = $encashment->userid;
        $charge->encashment_id = $encashment->id;
        $charge->save();
        }
        if ($encashment->transactionmethod == 2) {
            $userid = $encashment->userid;
            $requestFund = $encashment->amount;
            
            $updateFunds = userFunds::where('userid', $userid)->first();
            $regFunds = $updateFunds->registrationFunds;
        
            $finalRegFunds = $regFunds + $requestFund;
            $updateFunds->registrationFunds = $finalRegFunds;
            $updateFunds->save();
        }
        $user = $encash->userid;
        $user = User::find($user)->first();
        $mobileno = $request->mobileno;
        $mode = $request->encashmentmode;
        $releaseamount = $request->netamount;
        $firstname = $request->firstname;
        $lastname = $request->lastname;
        $date = $request->date;
        $sender = 'Jenny Lou C. Gutierrez';
        $message = '';
        $message .= 'ENCASHMENT: '.$mode."\r\n";
        $message .= 'SENDER: '.$sender."\r\n";
        $message .= ' RECEIVER: '.$firstname.' '.$lastname."\r\n";
        $message .= 'TRANSACTION DATE: '.$request->date."\r\n";
        $message .= 'Amount: PHP'.$strnet."\r\n";
        
        // dd($message);
        // $this->itexmo($mobileno,$message);
        $to = $request->email;
        $subject = "MaxHOPE Encashment Approved";
        $txt = '';
        $txt .= 'Dear '.$firstname.','. "\r\n\r\n";
        $txt .= '            MaxHOPE Marketing Services is pleased to inform you that your encashment request has been successfully approved.'. "\r\n\r\n";
        $txt .= 'Please see encashment details below:'. "\r\n\r\n";
        $txt .= 'Encashment              :'.$mode. "\r\n";
        $txt .= 'Sender                      :Jenny Lou C. Gutierrez'. "\r\n";
        $txt .= 'Receiver                    :'.$request->firstname.' '.$request->lastname. "\r\n";
        $txt .= 'Transaction Date       :'.$date. "\r\n";
        $txt .= 'MaxHOPE Ref. No.   :'.$request->id. "\r\n";
        $txt .= 'Amount                      :'.$releaseamount. "\r\n\r\n";
        $txt .= 'Thanks and regards,'. "\r\n\r\n";

        $txt .= 'MaxHOPE Marketing Services'. "\r\n";
        $txt .= 'Email Address: info@maxhopemrktgservices.com'. "\r\n";
        $txt .= 'Website: https://maxhopemrktgservices.com/'. "\r\n\r\n";

        $txt .= '****This is system generated email. No reply required****';   
                         
                    
        $headers = "From: info@maxhopemrktgservices.com" . "\r\n";

        // mail($to,$subject,$txt,$headers);
        return redirect('maintenance/payout/'.$id.'/edit')->with('message', 'Registration Fund Conversion / Encashment request has been successfully approved.');
    }
    private function itexmo($number,$message){
            $ch = curl_init();
            $itexmo = array('1' => $number, '2' => $message, '3' => 'PR-MAXH5706_EHRDC');
            curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
            curl_setopt($ch, CURLOPT_POST, 1);
             curl_setopt($ch, CURLOPT_POSTFIELDS, 
                      http_build_query($itexmo));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            return curl_exec ($ch);
            curl_close ($ch);
            //  $itexmo = array('1' => $number, '2' => $message, '3' => 'PR-MAXH5706_EHRDC');
            //  $url = 'https://www.itexmo.com/php_api/api.php';
            // $param = array(
            //     'http' => array(
            //         'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            //         'method'  => 'POST',
            //         'content' => http_build_query($itexmo),
            //     ),
            // );
            // $context  = stream_context_create($param);
            // return file_get_contents($url, false, $context);
    }
    private function activityLogs($description, $userid, $transactionid, $type){
        $logs = new activity_logs;
        $logs->description = $description;
        $logs->by  = $userid;
        $logs->transaction_id = $transactionid;
        $logs->type = $type;
        $logs->save();
        return 'sucess';
    }
    
}