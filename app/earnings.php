<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class earnings extends Model
{
    protected $table = 'earnings'; 
    protected $fillable = [
        'userid', 'earningTypeid', 'amount'
    ];
}
