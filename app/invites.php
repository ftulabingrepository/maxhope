<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invites extends Model
{
     protected $table = 'invites'; 
    protected $fillable = [
        'inviter', 'target'
    ];
}
