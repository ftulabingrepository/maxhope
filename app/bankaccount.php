<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bankaccount extends Model
{
    protected $table = 'bank_accounts'; 
    protected $fillable = [
        'name', 'number'
    ];
}
