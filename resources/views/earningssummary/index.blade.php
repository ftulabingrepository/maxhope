@extends('layouts.app')

@section('content')

<div class="main-content earning-summary-page">
    <section class="section">
      <h1 class="section-header">
        <div>Earnings Summary</div>
      </h1>
       
        <!--accounts area-->
        
        <div class="row">
            <div class="col-sm-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="ion ion-card"></i> Summary</h4>
                    </div>
                    <div class="card-body">
                        <table class="table summary-tbl">
                            <tbody>
                                @if(is_null($sysgen))
                                    @if(is_null($exit))
                                     <tr>
                                         <td><i class="ion ion-ios-circle-filled"></i> iAdvance Perks Table-1</td>
                                         <td style="text-align: right;">&#8369; 2,500.00</td>
                                     </tr>
                                    @endif
                                @endif
                                @foreach($earnings as $earning)
                                <tr>
                                    <td><i class="ion ion-ios-circle-filled"></i> {{$earning->type}}</td>
                                    @if($earning->tid == 2)
                                        @if(is_null($sysgen))
                                            <td style="text-align: right;">&#8369;{{number_format((int)$earning->total + 2500 , 2)}}</td>
                                        @else
                                            <td style="text-align: right;">&#8369;{{number_format($earning->total, 2)}}</td>
                                        @endif
                                       
                                    @else
                                    <td style="text-align: right;">&#8369;{{number_format($earning->total, 2)}}</td>
                                    @endif
                                </tr>
                                @endforeach
                                <tr>
                                    <td>TOTAL</td>
                                    @if(is_null($sysgen))
                                        <td style="text-align: right;">&#8369;{{number_format($total +2500, 2)}}</td>
                                    @else
                                        <td style="text-align: right;">&#8369;{{number_format($total, 2)}}</td>
                                    @endif
                                    
                                </tr>
                            </tbody>
                        </table>
                        

                    </div>
                </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
</div>

@endsection
@section('customjs')
<!-- <script src="{{asset('js/maintenance/funds.js')}}"></script -->
@endsection