<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class revenue extends Model
{
    protected $table = 'revenues'; 
    protected $fillable = [
        'amount', 'revenue_type', 'encashment_id'
    ];
}
