<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\tiers;
use App\levels;
use App\earningTypes;
use App\transaction_methods;
use App\tiergroups;
use App\encashmentmethod;
use App\encoded_funds;
use App\User;
use App\userFunds;
use App\encashmenttype;
use DB;
use Auth;
use App\activity_logs;
class MaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('maintenance.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function tiers(){
        $tiers = tiers::all();
        return view ('maintenance.tiers', compact('tiers'));
    }

    public function tiersdata(){
        
    }

    public function level(){
        $level = levels::all();
        return view ('maintenance.level', compact('level'));
    }

    public function leveldata(){
        
    }

    public function earningtypes(){
        $earningtypes = earningTypes::all();
       return view ('maintenance.earningtypes', compact('earningtypes'));
    }

    public function earningtypesdata(){
        
    }

    public function transactionmethod(){
        $transactionmethod = transaction_methods::all();
       return view ('maintenance.transactionmethod', compact('transactionmethod'));
    }

    public function transactionmethoddata(){
       
    }

    public function tiergroup(){
        $tiergroups = tiergroups::all();
       return view ('maintenance.tiergroup', compact('tiergroups'));
    }

    public function tiergroupdata(){
       dd('sdfsdf');
    }
    public function encashmentmethod(){
        
        return view('maintenance.encashmentmethod.index');
    }

    public function getEncashmentMethod(){
        $encashmethod = DB::table('encashmentmethods')->select('id', 'name')->get();
        return Datatables::of($encashmethod)
            ->setRowId(function ($encashmethod) {
                return $encashmethod->id;
            })
            ->addColumn('action', function ($encashmethod) {

                    return '<a href="encashmentmethod/'.$encashmethod->id.'/edit" class="btn btn-sm btn-primary"><i class="fa fa-edit" aria-hidden="true"></i>Edit</a>
                        <button id="mdelete" delete-id="'.$encashmethod->id.'" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i>Delete</button>
             
                    ';

            })
            ->make(true);
    }
    public function encashmentMethodAdd(Request $request){
        $types = encashmenttype::all();
        return view('maintenance.encashmentmethod.add', compact('types'));
        
    }
    public function encashmentMethodEdit(Request $request, $id){
        $method = DB::table('encashmentmethods')->first();
        return view('maintenance.encashmentmethod.edit', compact('method'));
        
    }
    public function encashmentMethodAddSubmit(Request $request){
        try {
            $request->validate([
                'name' => 'required|unique:encashmentmethods',
            ]);
            $encashmentmethod = new encashmentmethod;
            $encashmentmethod->name = $request->name;
            $encashmentmethod->type = $request->type;
            $encashmentmethod->save();
            return redirect()->back()->with('message', ''.$request->name.' encashment mode has been successfully added.');
        } catch (Exception $e) {
            return $e;
        }
    }
    public function encashmentMethodEditSubmit(Request $request, $id){
        try {
            $encashmentmethod = encashmentmethod::find($id);
            $encashmentmethod->name = $request->name;

            $encashmentmethod->save();
            return redirect()->back()->with('message', ''.$request->name.' encashment mode has been successfully edited.');
        } catch (Exception $e) {
            return $e;
        }
    }
    public function encashmentMethodDelete(Request $request){
        try {
            DB::table('encashmentmethods')->where('id', '=', $request->id)->delete();
            return 'success';
        } catch (Exception $e) {
            return $e;
        }
    }
    public function funds(){
        $userslist = User::all();
        return view('maintenance.funds.index', compact('userslist'));
    }
    public function fundsAdd(Request $request){
        $amount = number_format($request->amount, 2);
        $strpamount = str_replace(",", "", $amount);
        // dd($amount);
        if ($strpamount <= 0) {
            return redirect()->back()->with('error', "You can't add amount less than or equal 0.");
        }else{
            $userid = (int)$request->user;
            $user = DB::table('users')->where('id', $userid)->first();
            $check = DB::table('earnings')->where('userid', $userid)->where('earningTypeId', 2)->first();
            if (is_null($user)) {
            
                return redirect()->back()->with('error', 'Account does not exist.');
            }else{
                if(is_null($check)){
                    if ($strpamount >= 1580.00) {
                        $name = $user->firstname;
                        $lastname = $user->lastname;
                       
                        $regFunds = $this->getFunds($userid, 'registrationFunds');
                
                        
                        $addfunds = new encoded_funds;
                        $addfunds->userid = $userid;
                        $addfunds->amount = $strpamount;
                        $addfunds->save();
                        $funds = $regFunds + $strpamount;
                        DB::table('userfunds')
                            ->where('userid', $userid)
                            ->update(['registrationFunds' => $funds]);
                        $description = 'Admin added Registrtion Fund to '.$user->username.' total amount of ₱'.$strpamount.'.';
                        $transactionid = $addfunds->id;
                        $admin = Auth::user()->id;
                        $this->activityLogs($description, $admin, $transactionid, 3);
                        return redirect()->back()->with('message', 'Registration Fund of PHP '.$strpamount.' has been added to '.$user->username.'.');
                    }else{
                        // dd($amount);
                        return redirect()->back()->with('error', 'Registration not allowed.');
                    }
                }else{
                    return redirect()->back()->with('error', 'Username already exited and unable to receive registration fund.');
                }
                }
        }
        
    }
    public function getUsers(){
        // $userlist = User::all();
        // return 
    }
    private function getFunds($userid, $column){
        $regFunds = DB::table('userfunds')
                        ->where('userid', $userid)
                        ->first();

        if(!$regFunds){
            userFunds::create([
                'userid' => $userid,
                'registrationFunds' => 0,
                'payoutFunds' => 0
            ]);

            $this->getFunds($userid, $column);  
        }else{
            if($column == 'registrationFunds'){
                return $regFunds->registrationFunds;
            }elseif($column == 'payoutFunds'){
                return $regFunds->payoutFunds;
            }
        }
        
    }

    public function updatetiers(Request $request){
        $update = tiers::find($request->tierid);
        $update->tiername = $request->tiername;
        $update->levels = $request->levels;
        $update->save();
        return redirect('maintenance/tiers');
    }

    public function updatelevel(Request $request){
        $update = levels::find($request->levelid);
        $update->tierlevel = $request->_tierlevel;
        $update->slot = $request->_slot;
        $update->save();
        return redirect('maintenance/level');
    }

    public function updateearntypes(Request $request){
        $update = earningTypes::find($request->earntypeid);
        $update->type = $request->_type;
        $update->eAmount = $request->_eAmount;
        $update->save();
        return redirect('maintenance/earningtypes');
    }

    public function updatetranstype(Request $request){
        $update = transaction_methods::find($request->transtypeid);
        $update->method = $request->_method;
        $update->save();
        return redirect('maintenance/transactionmethod');
    }

    public function updatetiergroup(Request $request){
        $update = tiergroups::find($request->tiergroupid);
        $update->groupname = $request->_groupname;
        $update->save();
        return redirect('maintenance/tiergroup');
    }
    public function userFunds(){
        $userslist = User::all();
        return view('maintenance/funds/userfunds', compact('userslist'));
    }
    public function userFundsEdit($id){
        $user = DB::table('users as u')->select('u.id', 'u.firstname', 'u.lastname', 'u.username', 'uf.payoutFunds', 'uf.registrationFunds')->leftjoin('userfunds as uf', 'u.id', '=', 'uf.userid')->where('u.id', $id)->first();
        $earnings = DB::table('earnings as e')
            ->select(DB::raw('SUM(e.amount) as total'), 't.type', 't.id as tid')
            ->leftjoin('earning_types as t', 'e.earningTypeId', '=', 't.id')
            ->where('userid', $id)
            ->groupby('e.earningTypeId', 't.type', 'tid')
            ->get();
            $accumulated = DB::table('earnings as e')
        ->select(DB::raw('SUM(e.amount) as total'))
        ->where('userid', $id)
        ->first();
        $exit = DB::table('earnings')->where('userid', $id)->where('earningTypeId', 2)->first();
        $sysgen = DB::table('sys_accounts')->where('userid', $id)->first()  ;
        // dd($sysgen);
        $total = $accumulated->total;
        // dd($accumulated->total);
        return view('maintenance/funds/userfundsedit', compact('user', 'earnings', 'accumulated', 'exit', 'sysgen', 'total'));

    }
    public function userFundsEditSubmit(Request $request, $id){
        // dd($request->payoutFunds);
        $user = DB::table('users')->where('id', $id)->first();

        $registrationFunds = $request->registrationFunds;
        $payoutFunds = $request->payoutFunds;
        $description = 'Admin amended Wallet of '.$user->username.'.';
        $admin = Auth::user()->id;
        $this->activityLogs($description, $admin, 0, 4);
        
        DB::table('userfunds')->where('userid', $id)->update(['registrationFunds' => $registrationFunds, 'payoutFunds' => $payoutFunds]);
        
        return redirect()->back()->with('message', 'Funds Successfully Updated');
    }
    public function userFundsRedirect(Request $request){
        $check = DB::table('users')->where('id', $request->user)->first();
        if ($check) {
            $id = $request->user;
            return redirect('maintenance/user/funds/'.$id.'/edit');
        }else{
            return redirect()->back()->with('error', 'Username does not exist.');
        }
        
    }
    public function user($id){
        $user = DB::table('users')->where('id', $id)->first();

        return view('maintenance.userdetails', compact('user', 'id'));
    }
    private function activityLogs($description, $userid, $transactionid, $type){
        $logs = new activity_logs;
        $logs->description = $description;
        $logs->by  = $userid;
        $logs->transaction_id = $transactionid;
        $logs->type = $type;
        $logs->save();
        return 'sucess';
        //1 = encashment
        //2 = mps
        //3 = registration fund
        //4 = amend fund
    }
}
