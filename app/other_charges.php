<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class other_charges extends Model
{
     protected $table = 'other_charges'; 
    protected $fillable = [
        'userid', 'amount', 'encashment_id'
    ];
}
