$(document).ready(function(){
	var payout = $('#tbl-encashmentmethod').DataTable({
		processing: true,
		serverside: true,
		ajax: 'getencashmentmethod',
		columns: [
			{data: 'id', name: 'id'},
			{data: 'name', name: 'name'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
	});
	$( "#tbl-encashmentmethod tbody" ).on( "click", "#mdelete", function() {
	 var id = $(this).attr('delete-id');
	  	
	 var confirmation = confirm('Are you sure you want to delete this record?');
	 if (confirmation) {
	 	$.ajax({
	 		type: 'POST',
	 		url: 'encashmentmethod/delete',
	 		data: 'id='+id,
	 		success:function(data){
	 			location.reload();
	 		}
	 	});
	 }
	});
});