@extends('layouts.app')


@section('content')
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Create Month for MSP</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('createMSP') }}" aria-label="{{ __('MSP') }}">
              @csrf
        <div class="col-sm-12">
          <label>Month</label>
          <select name="month" class="form-control">
            <option disabled selected> - Select month - </option>
            <option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">June</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
          </select>
          <br />
          <label>Year</label>
          <input class="form-control" value="{{$year}}" name="year" type="number" required>
          <br />
   <!--        <label>Members</label>
          <input class="form-control" value="{{$eligible->count()}} Member/s are eligible for this Month" type="text" disabled> -->
        </div>
    
        <br />
 <button class="btn btn-info btn-block">ADD MPS</button>
      </form>        
        
      </div>
    </div>

  </div>
</div>

<div class="main-content earning-summary-page">
    <section class="section">
      <h1 class="section-header">
        <div>Monthly Profit Share</div>
      </h1>
        <!--accounts area-->
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="ion ion-card"></i> Months</h4>
                    </div>
                    <div class="card-body">
                      <div class="clearfix text-right">
                        <button class="btn btn-info" data-toggle="modal" data-target="#myModal" style="margin-bottom: 20px;">Add MPS</button>
                      </div>
                      @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top: 10px;">
                        <ul class="text-center">
                                <div class="alert alert-danger alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Error!</strong>  {{ $errors->first() }}
                                </div>
                        </ul>
                    </div>
                @endif
                 <div style="padding: 0 15px; overflow-x: auto;" class="tbl-datatables">
                    <table class="table table-bordered" id="tbl-mps" style="border-collapse: collapse!important;">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Month</th>
                              <th>Year</th>
                              <th>Qualified</th>
                              <th>Amount</th>
                              <th>Distributed</th>
                              <th>Created By</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                    </table>
                </div>

                    </div>
                </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
</div>

<!-- <div class="main-content earning-summary-page">
    <section class="section"> -->
        <!--accounts area-->
        
        <!-- <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="ion ion-card"></i>MSP Eligible Members for the month of {{$thisMonth}}</h4>
                    </div>
                    <div class="card-body">
                      <br />
                      <div class="clearfix">
                        <table class="table table-stripped">
                          <thead>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Username</th>
                          </thead>
                          <tbody> -->
                          <!-- @foreach($eligible as $e)
                            <tr>
                              <td>{{$e->firstname}}</td>
                              <td>{{$e->lastname}}</td>
                              <td>{{$e->username}}</td>
                            </tr>
                          @endforeach -->
                         <!--  </tbody>
                        </table>
                      </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--end accounts area-->
        <!--tables area-->
       
    <!-- </section> -->
</div>
@endsection

@section('customjs')
<script src="{{asset('js/mps/mps.js')}}"></script>
@endsection