@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>Encashment Method</div>
      </h1>
       
        <!--accounts area-->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="fa fa-edit"></i> Edit</h4>
                </div>
                <div class="card-body">
                    @if(session()->has('message'))
                      <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong>  {{ session()->get('message') }}
                      </div>
                    @elseif(session()->has('error'))
                      <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong>  {{ session()->get('error') }}
                      </div>
                    @endif
                    <form method="POST" action="edited">
                        @csrf
                        <label class="label" for="name">Encashment Method</label>
                        <input class="form-control btn-lg" name="name" id="name" type="text" placeholder="Name" required value="{{$method->name}}">
                        <div class="clearfix">
                            <div class="col-xs-6">
                                <button class="btn btn-primary btn-block" style="margin-top: 10px;" type="submit">Add Encashment Method</button>
                            </div>
                            
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
    
</div>

@endsection
@section('customjs')
<script src="{{asset('js/encashmentmethod/encashmentmethod.js')}}"></script>
@endsection