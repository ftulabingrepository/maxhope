@extends('layouts.app')

@section('content')
<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>Earning Types</div>
      </h1>
      <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="fa fa-list"></i> List</h4>
                </div>
                <div class="card-body">
                	<table class="table table-bordered" id="table-earningtypes">
						<thead>
							<tr>
								<th>Id</th>
								<th>Type</th>
								<th>Amount</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($earningtypes as $et)
								<tr>
									<td>{{ $et->id }}</td>
									<td>{{ $et->type }}</td>
									<td>{{ number_format($et->eAmount,2) }}</td>
									<td><button class="btn btn-sm btn-primary et" data-id="{{ $et->id }}"><i class="fa fa-edit"></i> Edit</button></td>
								</tr>
							@endforeach
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
      </section>
 </div>
  <!--modal-->
 <div  id="earntypemodal"class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Level</h5>
        <button type="button" class="close" onclick="$('#earntypemodal').modal('hide');" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="updateearntypes">
        @csrf
        <input type="hidden" name="earntypeid">
      <div class="modal-body">
      	<div class="form-group">
      		<label>Tier Level</label>
      		<input type="text" class="form-control" name="_type">
      	</div>
      	<div class="form-group">
      		<label>Slot</label>
      		<input type="number" class="form-control" name="_eAmount">
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="$('#earntypemodal').modal('hide');">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('customjs')
<script src="{{asset('js/maintenance/earningtypes.js')}}"></script>
@endsection