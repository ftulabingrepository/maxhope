<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert([
            'tierlevel' => 1,
            'slot' => 6
        ]);

        DB::table('levels')->insert([
            'tierlevel' => 2,
            'slot' => 4
        ]);

        DB::table('levels')->insert([
            'tierlevel' => 3,
            'slot' => 2
        ]);

        DB::table('levels')->insert([
            'tierlevel' => 4,
            'slot' => 1
        ]);

        DB::table('slots')->insert([
            'slotid' => 1,
            'tierlevelid' => 4,
            'slot' => 1
        ]);

        DB::table('slots')->insert([
            'slotid' => 2,
            'tierlevelid' => 3,
            'slot' => 2
        ]);

        DB::table('slots')->insert([
            'slotid' => 3,
            'tierlevelid' => 3,
            'slot' => 3
        ]);

        DB::table('slots')->insert([
            'slotid' => 4,
            'tierlevelid' => 2,
            'slot' => 4
        ]);

        DB::table('slots')->insert([
            'slotid' => 5,
            'tierlevelid' => 2,
            'slot' => 5
        ]);

        DB::table('slots')->insert([
            'slotid' => 6,
            'tierlevelid' => 2,
            'slot' => 6
        ]);

        DB::table('slots')->insert([
            'slotid' => 7,
            'tierlevelid' => 2,
            'slot' => 7
        ]);

        DB::table('slots')->insert([
            'slotid' => 8,
            'tierlevelid' => 1,
            'slot' => 8
        ]);

        DB::table('slots')->insert([
            'slotid' => 9,
            'tierlevelid' => 1,
            'slot' => 9
        ]);

        DB::table('slots')->insert([
            'slotid' => 10,
            'tierlevelid' => 1,
            'slot' => 10
        ]);

        DB::table('slots')->insert([
            'slotid' => 11,
            'tierlevelid' => 1,
            'slot' => 11
        ]);

        DB::table('slots')->insert([
            'slotid' => 12,
            'tierlevelid' => 1,
            'slot' => 12
        ]);

        DB::table('slots')->insert([
            'slotid' => 13,
            'tierlevelid' => 1,
            'slot' => 13
        ]);

        DB::table('tiers')->insert([
            'tiername' => 'table-1',
            'levels' => 4
        ]);

        DB::table('tiers')->insert([
            'tiername' => 'table-2',
            'levels' => 4
        ]);

        DB::table('tiers')->insert([
            'tiername' => 'table-3',
            'levels' => 4
        ]);

        DB::table('tiers')->insert([
            'tiername' => 'table-4',
            'levels' => 4
        ]);

        DB::table('tiers')->insert([
            'tiername' => 'table-5',
            'levels' => 4
        ]);

        DB::table('tiers')->insert([
            'tiername' => 'table-6',
            'levels' => 4
        ]);

        DB::table('earning_types')->insert([
            'type' => 'Monthly Sharing Pool',
            'eAmount' => 50
        ]);

        DB::table('earning_types')->insert([
            'type' => 'iAdvance Perks Table-1',
            'eAmount' => 4000
        ]);

        DB::table('earning_types')->insert([
            'type' => 'iAdvance Perks Table-2',
            'eAmount' => 11000
        ]);
        DB::table('earning_types')->insert([
            'type' => 'iAdvance Perks Table-3',
            'eAmount' => 40000
        ]);
        DB::table('earning_types')->insert([
            'type' => 'iAdvance Perks Table-4',
            'eAmount' => 120000
        ]);
        DB::table('earning_types')->insert([
            'type' => 'iAdvance Perks Table-5',
            'eAmount' => 600000
        ]);
        DB::table('earning_types')->insert([
            'type' => 'iAdvance Perks Table-6',
            'eAmount' => 900000
        ]);

        DB::table('earning_types')->insert([
            'type' => 'Indirect Referrals',
            'eAmount' => 5
        ]);

        DB::table('earning_types')->insert([
            'type' => 'Direct Referrals',
            'eAmount' => 50
        ]);

        DB::table('earning_types')->insert([
            'type' => 'MPS Status & Amount',
            'eAmount' => 50
        ]);

        DB::table('earning_types')->insert([
            'type' => 'CPS Status & Amount',
            'eAmount' => 50
        ]);

        DB::table('transaction_methods')->insert([
            'method' => 'Pay Out'
        ]);

        DB::table('transaction_methods')->insert([
            'method' => 'Registration Fund'
        ]);

        DB::table('users')->insert([
            'firstname' => 'Francis',
            'middlename' => 'Rubia',
            'lastname' => 'Tulabing',
            'address' => 'Villa Verna',
            'zipcode' => 6200,
            'mobileno' => '09055279059',
            'email' => 'ftulabing@gmail.com',
            'username' => 'ftulabing',
            'password' => bcrypt('123123'),
            'type' => 1
        ]);
        DB::table('users')->insert([
            'firstname' => 'Admin',
            'middlename' => 'Admin',
            'lastname' => 'Admin',
            'address' => 'Villa Verna',
            'zipcode' => 6200,
            'mobileno' => '0905349899',
            'email' => 'admin@gmail.com',
            'username' => 'admin',
            'password' => bcrypt('123123'),
            'type' => 1
        ]);
        DB::table('encashment_status')->insert([
            'status' => 'Pending'
        ]);
        DB::table('encashment_status')->insert([
            'status' => 'Approved'
        ]);
        DB::table('revenue_types')->insert([
            'name' => 'encash',
            'percent' => 6
        ]);
        DB::table('revenue_types')->insert([
            'name' => 'conver registration fund',
            'percent' => 3
        ]);
        DB::table('revenue_types')->insert([
            'name' => 'tax',
            'percent' => 6
        ]);
        DB::table('revenue_types')->insert([
            'name' => 'admin fee',
            'percent' => 3
        ]);
        DB::table('revenue_types')->insert([
            'name' => 'others',
            'percent' => 3
        ]);

        //modules
        DB::table('modules')->insert([
            'name' => 'Dashboard',
            'url' => '/dashboard',
            'icon' => 'ion ion-speedometer',
            'parent' => 0,
            'has_sub' => 0
        ]);
        DB::table('modules')->insert([
            'name' => 'Payout',
            'url' => '/payout',
            'icon' => 'fa fa-money-bill-alt',
            'parent' => 0,
            'has_sub' => 0
        ]);
        DB::table('modules')->insert([
            'name' => 'Register',
            'url' => '/register',
            'icon' => 'fa fa-book',
            'parent' => 0,
            'has_sub' => 0
        ]);
        
        DB::table('modules')->insert([
            'name' => 'Maintenance',
            'url' => '/maintenance',
            'icon' => 'ion ion-ios-albums-outline',
            'parent' => 0,
            'has_sub' => 1
        ]);
        DB::table('modules')->insert([
            'name' => 'Tiers',
            'url' => '/maintenance/tiers',
            'icon' => 'ion ion-ios-circle-outline',
            'parent' => 4,
            'has_sub' => 0
        ]);
        DB::table('modules')->insert([
            'name' => 'Level',
            'url' => '/maintenance/level',
            'icon' => 'ion ion-ios-circle-outline',
            'parent' => 4,
            'has_sub' => 0
        ]);
        DB::table('modules')->insert([
            'name' => 'Earning Types',
            'url' => '/maintenance/earningtypes',
            'icon' => 'ion ion-ios-circle-outline',
            'parent' => 4,
            'has_sub' => 0
        ]);
        DB::table('modules')->insert([
            'name' => 'Transaction Type',
            'url' => '/maintenance/transactionmethod',
            'icon' => 'ion ion-ios-circle-outline',
            'parent' => 4,
            'has_sub' => 0
        ]);
        DB::table('modules')->insert([
            'name' => 'Tier Group',
            'url' => '/maintenance/tiergroup',
            'icon' => 'ion ion-ios-circle-outline',
            'parent' => 4,
            'has_sub' => 0
        ]);
        DB::table('modules')->insert([
            'name' => 'Encashment Method',
            'url' => '/maintenance/encashmentmethod',
            'icon' => 'ion ion-ios-circle-outline',
            'parent' => 4,
            'has_sub' => 0
        ]);
        DB::table('modules')->insert([
            'name' => 'Funds',
            'url' => '/maintenance/funds',
            'icon' => 'ion ion-ios-circle-outline',
            'parent' => 4,
            'has_sub' => 0
        ]);
        DB::table('modules')->insert([
            'name' => 'Accounts',
            'url' => '/accounts',
            'icon' => 'fa fa-user',
            'parent' => 0,
            'has_sub' => 0
        ]);
        DB::table('modules')->insert([
            'name' => 'MPS',
            'url' => '/msp',
            'icon' => 'fa fa-user',
            'parent' => 0,
            'has_sub' => 0
        ]);
        //module access
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 1,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 2,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 3,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 4,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 5,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 6,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 7,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 8,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 9,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 10,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 11,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 1,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 2,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 3,
            'grant_access' =>0 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 4,
            'grant_access' => 0 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 5,
            'grant_access' => 0
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 6,
            'grant_access' => 0 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 7,
            'grant_access' => 0 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 8,
            'grant_access' => 0 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 9,
            'grant_access' => 0 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 10,
            'grant_access' => 0 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 11,
            'grant_access' => 0 
        ]);
        DB::table('encashment_types')->insert([
            'name' => 'Bank'
        ]);
        DB::table('encashment_types')->insert([
            'name' => 'Padala'
        ]);

        DB::table('modules_accesses')->insert([
            'usertype' => 2,
            'module_id' => 12,
            'grant_access' => 1 
        ]);
        DB::table('modules_accesses')->insert([
            'usertype' => 1,
            'module_id' => 13,
            'grant_access' => 1 
        ]);
    }
}
