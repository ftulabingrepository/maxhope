$(document).ready(function(){
	var mps = $('#tbl-mps').DataTable({
		processing: true,
		serverside: true,
		ajax: 'msp/getmsp',
		columns: [
			{data: 'mid', name: 'mid'},
			{data: 'Month', name: 'Month'},
			{data: 'year', name: 'year'},
			{data: 'member_count', name: 'member_count'},
			{data: 'amount', name: 'amount'},
			{data: 'distributed', name: 'distributed'},
			{data: 'username', name: 'username'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
	});
	var mps = $('#tbl-qualify').DataTable({
		processing: true,
		serverside: true,
		ajax: 'getqualified',
		columns: [
			{data: 'username', name: 'username'},
			{data: 'firstname', name: 'firstname'},
			{data: 'lastname', name: 'lastname'},
		]
	});
});