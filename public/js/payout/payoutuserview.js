$(document).ready(function(){
	var payout = $('#tbl-payout').DataTable({
		processing: true,
		serverside: true,
		ajax: 'getpayout',
		columns: [
			{data: 'id', name: 'id'},
			{data: 'created_at', name: 'create_at'},
			{data: 'username', name: 'username'},
			{data: 'firstname', name: 'firstname'},
			{data: 'middlename', name: 'middlename'},
			{data: 'lastname', name: 'lastname'},
			{data: 'amount', name: 'amount'},
			{data: 'tax', name: 'tax'},
			{data: 'adminfee', name: 'adminfee'},
			{data: 'othercharges', name: 'othercharges'},
			{data: 'net', name: 'net'},
			{data: 'name', name: 'name'},
			{data: 'method', name: 'method'},
			{data: 'status', name: 'status'}
			
			
		]
	});
	
	
});