<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use DB;

class access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next , $guard = null)
    {
        $checker = Auth::guard($guard)->check();
        if(!$checker){
            abort(401, 'You are not login, please login first, or sign up for an account to proceed!');
        }else{
            $routeName = explode("/", $request->route()->uri);
            $routeName = $routeName[0];
            // dd($routeName);
            $modulesaccess = DB::table('modules_accesses as ma')->join('modules as m', 'ma.module_id', '=', 'm.id')->where('name', $routeName)->where('usertype', Auth::user()->type)->first();
            // dd($modulesaccess);
            if(!$modulesaccess){
                return $next($request);
            }
            else if($modulesaccess->grant_access == 0){
                abort(403, "You're not allowed to access this page!");
            }else{
                return $next($request);
            }
            // // dd(Auth::user()->type);
            // if(count($modulesaccess) > 0){
            //     dd("granted");
            // }else{
            //     dd("denied");
            // }
        }
    }
}
