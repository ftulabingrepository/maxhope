@extends('layouts.app')


@section('content')

<div class="main-content earning-summary-page">
    <section class="section">
      <h1 class="section-header">
        <div>System Accounts</div>
      </h1>
       
        <!--accounts area-->
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <!-- <h4><i class="ion ion-card"></i> My Accounts</h4> -->
                    </div>
                    <div class="card-body">

                      <table class="table table-stripped" id="tbl-accounts" style="border-collapse: collapse;">
                        <thead>
                          <th>#</th>
                          <th>Username</th>
                          <th>First name</th>
                          <th>Middle name</th>
                          <th>Last name</th>
                          <th>Date Generated</th>
                          <th>Action</th>
                        </thead>
                        
                      </table>

                    </div>
                </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
</div>
@endsection

@section('customjs')
<script src="{{asset('js/accounts/accounts.js')}}"></script>
@endsection