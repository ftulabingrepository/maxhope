<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class encashment_status extends Model
{
    protected $table = 'encashment_status'; 
    protected $fillable = [
        'id', 'status'
    ];
}
