<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\bankaccount;
use DB;
use recipient;
class ProfileController extends Controller
{
    public function view(){
    	$userid = Auth::user()->id;
    	$user = DB::table('users as u')->leftjoin('bank_accounts as b', 'u.id', '=', 'b.userid')->where('u.id', $userid)->first();
    	// $bnkacnt = bankaccount::where('userid', $userid)->first();
        $recipient = DB::table('recipients')->where('userid', $userid)->first();
        $checksysadd = DB::table('sys_accounts')->where('userid', $userid)->count();
        if ($checksysadd > 0) {
            $readonly = 'readonly';
        }else{
            $readonly = '';
        }
    	return view('profile', compact('user', 'recipient', 'checksysadd', 'userid'));
    }
    public function edit(Request $request){

        $users = User::find($request->id);
        $users->firstname = $request->firstname;
        $users->middlename = $request->middlename;
        $users->lastname = $request->lastname;
        $users->suffix = $request->suffix;
        $users->address = $request->address;
        $users->zipcode = $request->zipcode;
        $users->username = $request->username;
        $users->mobileno = $request->mobileno;
        $users->email = $request->email;
        $users->type = 2;
        $users->save();
        return redirect()->back()->with('message', 'Profile Successfully Updated.');
    }
    public function update(Request $request){
    	$userid = Auth::user()->id;
    	$bankacnt = bankaccount::where('userid', $userid)->first();
    	try {
    		if (is_null($bankacnt)) {
    			$name = $request->accountname;
		    	$number = (string)$request->accountnumber;
                $bankname = $request->bankname;
		    	$acnt = new bankaccount;
		    	$acnt->userid = $userid; 
		    	$acnt->name = $name;
		    	$acnt->number = $number;
                $acnt->bankname = $request->bankname;
		    	$acnt->save();
	    		return redirect()->back()->with('message', 'Bank account successfully added.');
    		}else{
    			$name = $request->accountname;
		    	$number = (string)$request->accountnumber;
                $bankname = $request->bankname;
		    	$acnt =  bankaccount::where('userid',$userid);
		    	DB::table('bank_accounts')->where('userid', $userid)->update(['name' => $name, 'number' => $number, 'bankname' => $bankname]);
	    		return redirect()->back()->with('message', 'Bank account successfully updated.');
    		}
    		
    	} catch (Exception $e) {
    		return redirect()->back()->with('error', $e);
    	}
    	
    }
    public function recipientAdd(Request $request){
        $userid = Auth::user()->id;
        $recipient = new \App\recipient;
        $recipient->userid = $userid;
        $recipient->firstname = $request->firstname;
        $recipient->middlename = $request->middlename;
        $recipient->lastname = $request->lastname;
        $recipient->address = $request->address;
        $recipient->mobileno = $request->mobileno;
        $recipient->save();
        return redirect()->back()->with('message', 'Recipient successfully added.');
    }
    public function recipientEdit(Request $request){
        $userid = Auth::user()->id;
        // $recipient = recipient::where('userid', $userid);
        DB::table('recipients')->where('userid', $userid)->update(['firstname' => $request->firstname, 'lastname' => $request->lastname, 'middlename' => $request->middlename, 'address' => $request->address, 'mobileno' => $request->mobileno]);
        
        return redirect()->back()->with('message', 'Recipient successfully updated.');
    }

}
