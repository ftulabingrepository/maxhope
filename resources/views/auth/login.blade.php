@extends('layouts.app1')

@section('content')
<div style="height: 100%; background-image: url('../img/bg-login.jpg'); background-size: cover; ">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">

            <div class="card page-login">
                

                <div class="card-body">
                    <img src="{{asset('img/logo.jpg')}}" class="logo" atl="Company Logo">
                    <h4>Welcome to <span class="primary-color">MaxHOPE</span> Marketing Services</h4>
                    <p>Our Company profile defines who we are, what we intend to achieve, our vision for the future and how we give value to our clients and affiliates.</p>
                    <h5><i class="fa fa-user"></i> Login to My Account</h5>
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group">
                        
                            
                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus placeholder="Username">

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                          
                        </div>

                        <div class="form-group">
                            
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <p>Copyright &copy; 2018 <span class="primary-color">MaxHOPE Marketing Services</span></p>
                <p>All Rights Reserved.</p>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
