<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class referals extends Model
{
    protected $table = 'referals'; 
    protected $fillable = [
        'userid', 'refered'
    ];
}
