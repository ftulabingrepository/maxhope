@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>User Information</div>
      </h1>
       
        <!--accounts area-->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="ion ion-card"></i> Information</h4>
                <!-- <a href="maintenance/user/funds/{{$id}}/edit" class="btn btn-primary btn-add" style="border-radius: 5px;">Amend Funds</a> -->
                </div>
                <div class="card-body">
                    @if(session()->has('message'))
                      <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                         {{ session()->get('message') }}
                      </div>
                    @elseif(session()->has('error'))
                      <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong>  {{ session()->get('error') }}
                      </div>
                    @endif
                    <form method="POST" action="{{URL::to('profile/edit')}}">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <input type="hidden" name="id" value="{{$id}}">
                                <div class="form-group">
                                    <label for="username">User Name</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->username}}" readonly name="username">
                                </div>
                                <div class="form-group">
                                    <label for="username">First Name</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->firstname}}"  name="firstname">
                                </div>
                                <div class="form-group">
                                    <label for="username">Middle Name</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->middlename}}"  name="middlename">
                                </div>
                                <div class="form-group">
                                    <label for="username">Last Name</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->lastname}}"  name="lastname">
                                </div>
                                <div class="form-group">
                                    <label for="username">Suffix</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->suffix}}"  name="suffix">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="username">Address</label>
                                    <textarea class="form-control"  name="address">{{$user->address}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="username">Zipcode</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->zipcode}}"  name="zipcode">
                                </div>
                                <div class="form-group">
                                    <label for="username">Mobile Number</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->mobileno}}"  name="mobileno">
                                </div>
                                <div class="form-group">
                                    <label for="username">Email Address</label>
                                    <input type="text" class="form-control" id="username" value="{{$user->email}}"  name="email">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary float-right">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
</div>

@endsection
@section('customjs')

@endsection