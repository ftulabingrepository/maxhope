@extends('layouts.app')

@section('content')
<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>Tier Groups</div>
      </h1>
      <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="fa fa-list"></i> List</h4>
                </div>
                <div class="card-body">
                	<table class="table table-bordered" id="table-tiergroup">
						<thead>
							<tr>
								<th>Group Id</th>
								<th>Group Name</th>
								<th>Tier Id</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($tiergroups as $tg)
								<tr>
									<td>{{ $tg->groupid }}</td>
									<td>{{ $tg->groupname }}</td>
									<td>{{ $tg->tierid }}</td>
									<td><button class="btn btn-sm btn-primary tg" data-id="{{ $tg->id }}"><i class="fa fa-edit"></i> Edit</button></td>
								</tr>
							@endforeach
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
      </section>
 </div>
 <div  id="tiergroupmodal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tier Group</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="$('#tiergroupmodal').modal('hide')" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="updatetiergroup">
        @csrf
        <input type="hidden" name="tiergroupid">
      <div class="modal-body">
        <div class="form-group">
        	<label>Group Name</label>
        	<input type="text" name="_groupname" class="form-control">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="$('#tiergroupmodal').modal('hide')">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('customjs')
<script src="{{asset('js/maintenance/tiergroup.js')}}"></script>
@endsection