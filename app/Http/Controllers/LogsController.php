<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\activity_logs;
use Yajra\DataTables\Facades\DataTables;
class LogsController extends Controller
{
    public function index(){
    	return view('logs/index');
    }
    public function getlogs(){
    	$logs = activity_logs::all();
    	return Datatables::of($logs)
                    ->setRowId(function ($logs) {
                        return $logs->id;
                    })
                    ->make(true);
    }
}
