@extends('layouts.app')


@section('content')

<div class="main-content earning-summary-page">
    <section class="section">
      <h1 class="section-header">
        <div>System Accounts</div>
      </h1>
       
        <!--accounts area-->
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="ion ion-card"></i> My Accounts</h4>
                    </div>
                    <div class="card-body">

                      <table class="table table-stripped">
                        <thead>
                          <th>#</th>
                          <th>Username</th>
                          <th>Last name</th>
                          <th>First name</th>
                          <th>Middle name</th>
                          <th>Date Generated</th>
                          <th>Action</th>
                        </thead>
                        <tbody>
                          @if($ownerRec)
                            @if(Auth::user()->id != $ownerRec->id)
                              <tr>
                                <td></td>
                                <td>{{$ownerRec->username}}</td>
                                <td>{{$ownerRec->lastname}}</td>
                                <td>{{$ownerRec->firstname}}</td>
                                <td>{{$ownerRec->middlename}}</td>
                                <td>{{$ownerRec->created_at}}</td>
                                <td>
                                  <form method="POST" action="{{ route('logoutAcc') }}">
                                    @csrf<button class="btn btn-info" name="btnID" value="{{$ownerRec->id}}"> Login</button></form></td>
                              </tr>
                            @endif
                          @endif
                          @foreach($accounts as $account)
                          @if(Auth::user()->id != $account->id)
                          <tr>
                            <td></td>
                            <td>{{$account->username}}</td>
                            <td>{{$account->lastname}}</td>
                            <td>{{$account->firstname}}</td>
                            <td>{{$account->middlename}}</td>
                            <td>{{$account->created_at}}</td>
                            <td>
                              <form method="POST" action="{{ route('logoutAcc') }}">
                                @csrf<button class="btn btn-info" name="btnID" value="{{$account->id}}"> Login</button></form></td>
                          </tr>
                          @endif
                          @endforeach
                        </tbody>
                      </table>

                    </div>
                </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
</div>
@endsection

@section('customjs')
@endsection