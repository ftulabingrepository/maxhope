@extends('layouts.app')

@section('content')

<div class="main-content earning-summary-page">
    <section class="section">
      <h1 class="section-header">
        <div>Pay Out</div>
      </h1>
       
        <!--accounts area-->
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="ion ion-card"></i> Summary</h4>
                    </div>
                    <div class="card-body">
                        
                        

                    </div>
                </div>
            </div>
        </div>
        <!--end accounts area-->
        <!--tables area-->
       
    </section>
    <div style="padding: 0 15px; overflow-x: auto;" class="tbl-datatables">
        <table class="table table-bordered" id="tbl-payout" style="border-collapse: collapse!important;">
                            <thead>
                                @if($usertype == 1)
                                  <tr class="total-header">    
                                    <th colspan="6">Grand Total</th>
                                    <th>&#8369;{{$total->totalamount}}</th>
                                    <th>&#8369;{{$tax->totaltax}}</th>
                                    <th>&#8369;{{$adminfee->totaladminfee}}</th>
                                    <th>&#8369;{{$charges->totalcharges}}</th>
                                    <th>&#8369;{{$total->totalnet}}</th>
                                    
                                </tr>
                                @endif
                                
                                <tr>
                                    <th>ID</th>
                                    <th>Date</th>
                                    <th>Username</th>
                                    <th>First Name</th>
                                    <th>Middle Name</th>
                                    <th>Last Name</th> 
                                    <th>Amount</th>
                                    <th>Withholding Tax</th>
                                    <th>Admin Fee</th>
                                    <th>Other Charges</th>
                                    <th>Net</th>
                                    <th>Method</th>
                                    <th>Transaction</th>
                                    <th>Status</th>
                                    @if($usertype == 1)
                                        <th>Action</th>
                                    @endif
                                    
                                </tr>
                            </thead>
                        </table>
    </div>
</div>

@endsection
@section('customjs')
    @if($usertype == 1)
     <script src="{{asset('js/payout/payout.js')}}"></script>
        
    @else
       <script src="{{asset('js/payout/payoutuserview.js')}}"></script>
    @endif

@endsection
<style type="text/css">
    .total-header{
        background-color: #fff;
    }
    .total-header th{

        border: 2px solid #0275d8!important;
    }
</style>