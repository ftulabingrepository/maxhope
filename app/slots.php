<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slots extends Model
{
    protected $table = 'slots'; 
    protected $fillable = [
        'slotid', 'tierlevelid', 'slot'
    ];
}
