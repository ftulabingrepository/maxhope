@extends('layouts.app')

@section('content')
<div class="main-content">
    <section class="section">
      <h1 class="section-header">
        <div>Transaction Type</div>
      </h1>
      <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                    <h4><i class="fa fa-list"></i> List</h4>
                </div>
                <div class="card-body">
                	<table class="table table-bordered" id="table-transact">
						<thead>
							<tr>
								<th>Id</th>
								<th>Method</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($transactionmethod as $tm)
								<tr>
									<td>{{ $tm->id }}</td>
									<td>{{ $tm->method }}</td>
									<td><button class="btn btn-sm btn-primary tm" data-id="{{ $tm->id }}"><i class="fa fa-edit"></i> Edit</button></td>
								</tr>
							@endforeach
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
      </section>
 </div>
 <!--modal-->
 <div  id="transactmethodmodal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Transaction Method</h5>
        <button type="button" class="close" onclick="$('#transactmethodmodal').modal('hide');" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="get" action="updatetranstype">
        @csrf
        <input type="hidden" name="transtypeid">
      <div class="modal-body">
      	<div class="form-group">
      		<label>Tier Level</label>
      		<input type="text" class="form-control" name="_method">
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="$('#transactmethodmodal').modal('hide');">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('customjs')
<script src="{{asset('js/maintenance/transactionmethod.js')}}"></script>
@endsection