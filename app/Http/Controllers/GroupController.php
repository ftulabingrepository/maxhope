<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tiergroups;
use App\movements;
use App\User, App\userFunds;
use Auth;

class GroupController extends Controller
{
    //
    public function createGroup(){
    	return view('createGroup');
    }

    public function addGroup(Request $request){
    	$userid = Auth::user()->id;

    	$newGroup = tiergroups::create([
			    		'groupid' => 1,
			    		'groupname' => $request->groupname,
			    		'tierid' => 1,
			    		'parentid' => 0,
                        'ancestor' => 0,
                        'left' => 0,
                        'right' => 0,
			    		'status' => 'Moving'
			    	]);

    	// if($newGroup){
     //        $newUser = $this->addNewUserReturnId($request);
     //        if($newUser){
     //            $this->newMovement($newUser, $newGroup->id);
     //        }	
    	// }

        if($newGroup){
            $this->newMovement($userid, $newGroup->id);
        }   

    	return redirect('dashboard');
    }

    private function addNewUserReturnId($request){
        $userid = Auth::user()->id;
        $user = new User;
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->address = $request->address;
        $user->zipcode = $request->zipcode;
        $user->mobileno = $request->mobile;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->type = 2;
        $saved = $user->save();

        $defaultFunds = new userFunds;
        $defaultFunds->userid = $user->id;
        $defaultFunds->registrationFunds = 0;
        $defaultFunds->payoutFunds = 2500;
        $defaultFunds->save();

        return $user->id;
    }

    private function newMovement($newUserID, $groupid){
        $newUserMovement = new movements;
        $newUserMovement->groupid = $groupid;
        $newUserMovement->userid = $newUserID;
        $newUserMovement->movedto = 13;
        $newUserMovement->origin = 0;
        $newUserMovement->active = 1;
        $newUserMovement->save();

        for($x = 1; $x < 13; $x++){
        	$newUserMovement = new movements;
	        $newUserMovement->groupid = $groupid;
	        $newUserMovement->userid = 0;
	        $newUserMovement->movedto = $x;
	        $newUserMovement->origin = 0;
	        $newUserMovement->active = 1;
	        $newUserMovement->save();
        }
	}

}
