<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class recipient extends Model
{
    protected $table = 'recipients';
    protected $fillable = [
    	'fistname', 'lastname', 'userid', 'middlename', 'address', 'mobileno'
    ];
    
}


