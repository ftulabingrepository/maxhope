<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tiergroups extends Model
{
    protected $table = 'tiergroups'; 
    protected $fillable = [
        'groupid', 'groupname', 'tierid', 'parentid', 'ancestor', 'left', 'right', 'status'
    ];
}
