@extends('layouts.app')

@section('content')
<div class="main-content">
        <section class="section">
          <h1 class="section-header">
            <div>Home</div>
          </h1>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Greetings!</div>

                            <div class="card-body">
                                <h3>Welcome to <span class="primary-color">MaxHOPE</span> {{$name}} {{$lastname}}!</h3>

                            </div>
                        </div>
                        <img src="{{asset('img/logo.png')}}" class="logo" atl="Company Logo" style="margin: 20px auto; display: block;">
                    </div>
                </div>
            </div>
 </section>
</div>
@endsection
