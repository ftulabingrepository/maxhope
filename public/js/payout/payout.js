$(document).ready(function(){
	var payout = $('#tbl-payout').DataTable({
		processing: true,
		serverside: true,
		ajax: 'getpayout',
		columns: [
			{data: 'id', name: 'id'},
			{data: 'created_at', name: 'create_at'},
			{data: 'username', name: 'username'},
			{data: 'firstname', name: 'firstname'},
			{data: 'middlename', name: 'middlename'},
			{data: 'lastname', name: 'lastname'},
			{data: 'amount', name: 'amount'},
			{data: 'tax', name: 'tax'},
			{data: 'adminfee', name: 'adminfee'},
			{data: 'othercharges', name: 'othercharges'},
			{data: 'net', name: 'net'},
			{data: 'name', name: 'name'},
			{data: 'method', name: 'method'},
			{data: 'status', name: 'status'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
	});
	// $.ajax({
	// 	type:'get',
	// 	url:'gettotalpayout',
	// 	success: function(data){
	// 			payout.rows.add(data);
 //        		payout.draw();
	// 	}
	// });
	$('#compute').click(function(){
		var charge = Number($('#otherCharges').val());
		if (charge != '') {
			if (charge > 0 ) {
				var charges = charge.toFixed(2);
				var currency = $('#netAmount').val();
				var number = Number(currency.replace(/[^0-9.-]+/g,""));
				var amount = number.toFixed(2);
				// alert(number);
				// var amounts = amount.toFixed(2);
				var net = amount - charges;
				var finalnet = net.toFixed(2);
				var peso = '₱';
				$('#netAmountDisplay').val(peso+finalnet);
			}else{
				alert('Charge amount is invalid. Please enter correct figure.');
			}
		}
		
	});
	
});