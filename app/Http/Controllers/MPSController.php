<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, DB, Redirect;
use App\msp, App\invites, App\User, App\earningTypes, App\userFunds, App\earnings;
use Carbon;
use App\msps_details;
use App\activity_logs;
use Yajra\DataTables\Facades\DataTables;
class MPSController extends Controller
{

    public function index(){
    	$dt = Carbon::now();
    	$year = $dt->year;
        $thisMonth = $dt->format('F');
        $dt1 = Carbon::create($year, $dt->month, 1, 0, 0, 0, 'Asia/Manila');
        $dt2 = $carbon = new Carbon('last day of '.$thisMonth.' '.$dt->year.'');
        
    	$en = DB::table('msps as m')
                    ->select(DB::raw('(CASE WHEN m.month = 1 THEN "January" WHEN m.month = 2 THEN "February" WHEN m.month = 3 THEN "March" WHEN m.month = 4 THEN "April" WHEN m.month = 5 THEN "May" WHEN m.month = 6 THEN "June" WHEN m.month = 7 THEN "July" WHEN m.month = 8 THEN "August" WHEN m.month = 9 THEN "September" WHEN m.month = 10 THEN "October" WHEN m.month = 11 THEN "November" WHEN m.month = 12 THEN "December" END) as Month'), 'm.year', 'm.member_count', 'm.distributed', 'u.lastname', 'u.firstname', 'm.created_at', 'm.id as mid', 'u.username', DB::raw('(CASE WHEN m.distributed = 0 THEN "NO" ELSE "YES" END)'))
                    ->leftJoin('users as u', 'm.by', '=', 'u.id')
                    ->orderBy('m.id', 'desc')
                    ->get();
                    // dd($en);

    	$eligible = invites::select(DB::raw('DISTINCT inviter'), 'users.firstname', 'users.lastname', 'users.username')
                    ->leftJoin('users', 'invites.inviter', '=', 'users.id')
                    ->groupBy('invites.inviter', 'users.firstname', 'users.lastname', 'users.username')
                    ->havingRaw('COUNT(inviter) >= 5')
                    ->where('invites.created_at', '>', $dt1)
                    ->where('invites.created_at', '<', $dt2)
                    ->get();
        
        
     	return view('mps', compact('eligible', 'year', 'thisMonth'));
    }
    public function edit($id){
        $mps = DB::table('msps as m')->select(DB::raw('(CASE WHEN m.month = 1 THEN "January" WHEN m.month = 2 THEN "February" WHEN m.month = 3 THEN "March" WHEN m.month = 4 THEN "April" WHEN m.month = 5 THEN "May" WHEN m.month = 6 THEN "June" WHEN m.month = 7 THEN "July" WHEN m.month = 8 THEN "August" WHEN m.month = 9 THEN "September" WHEN m.month = 10 THEN "October" WHEN m.month = 11 THEN "November" WHEN m.month = 12 THEN "December" END) as Month'), 'm.year', 'm.member_count', 'u.lastname', 'u.firstname', 'm.created_at', 'm.id as mid', 'u.username', DB::raw('(CASE WHEN m.distributed = 0 THEN "NO" ELSE "YES" END) as distributed'), 'm.amount')->leftJoin('users as u', 'm.by', '=', 'u.id')->where('m.id',$id)->first();
        return view('mps.edit', compact('mps'));
    }
    public function editMsp($id){
        $msp = msp::find($id);
        if ($msp->distributed != 1) {
            $qualify = DB::table('msps_details')->where('msps_id', $id)->get();
        foreach ($qualify as $qual) {
            $this->addEarnings($qual->qualified, 10, $msp->amount);
        }

        $msp->distributed = 1;
        $msp->save();
        $month = $msp->month;
        $monthName = 'January';
        if($month == 2){
            $monthName = 'February';
        }elseif($month == 3){
            $monthName = 'March';
        }elseif($month == 4){
            $monthName = 'April';
        }elseif($month == 5){
            $monthName = 'May';
        }elseif($month == 6){
            $monthName = 'June';
        }elseif($month == 7){
            $monthName = 'July';
        }elseif($month == 8){
            $monthName = 'August';
        }elseif($month == 9){
            $monthName = 'September';
        }elseif($month == 10){
            $monthName = 'October';
        }elseif($month == 11){
            $monthName = 'November';
        }elseif($month == 12){
            $monthName = 'December';
        }
        $username = Auth::user()->username;
        $userid = Auth::user()->id;
        $description = $username.' distributed mps of '.$monthName.' '.$msp->year;
        $this->activityLogs($description, $userid, $id);
        return redirect()->back()->with('message', 'Successfully Distributed');
        }
        
    }
    public function getqualified($id){
        $en = DB::table('msps_details as mp')->where('msps_id', $id)->leftjoin('users as u','mp.qualified', '=', 'u.id')->get();
        return Datatables::of($en)
            ->setRowId(function ($en) {
                return $en->id;
            })
            ->make(true);
    }
     public function getmps(){
        $en = DB::table('msps as m')
                    ->select(DB::raw('(CASE WHEN m.month = 1 THEN "January" WHEN m.month = 2 THEN "February" WHEN m.month = 3 THEN "March" WHEN m.month = 4 THEN "April" WHEN m.month = 5 THEN "May" WHEN m.month = 6 THEN "June" WHEN m.month = 7 THEN "July" WHEN m.month = 8 THEN "August" WHEN m.month = 9 THEN "September" WHEN m.month = 10 THEN "October" WHEN m.month = 11 THEN "November" WHEN m.month = 12 THEN "December" END) as Month'), 'm.year', 'm.member_count', 'u.lastname', 'u.firstname', 'm.created_at', 'm.id as mid', 'u.username', DB::raw('(CASE WHEN m.distributed = 0 THEN "NO" ELSE "YES" END) as distributed'), 'm.amount')
                    ->leftJoin('users as u', 'm.by', '=', 'u.id')
                    ->orderBy('m.id', 'desc')
                    ->get();

         return Datatables::of($en)
            ->setRowId(function ($en) {
                return $en->mid;
            })
            ->addColumn('action', function ($en) {

                    return '<a href="msp/'.$en->mid.'/edit" class="btn btn-sm btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> View</a>';

            })
            ->make(true);
    }
    public function addMonth(Request $request){
        $mDone = msp::where('month', $request->month)->where('year', $request->year)->count();

        if($mDone > 0){
            return Redirect::back()->withErrors(['MPS for the month selected is already distributed.']);
        }

    	$userid = Auth::user()->id;
    	$monthName = 'January';
    	$msp = earningTypes::find(10);
    	

    	if($request->month == 2){
    		$monthName = 'February';
    	}elseif($request->month == 3){
    		$monthName = 'March';
    	}elseif($request->month == 4){
    		$monthName = 'April';
    	}elseif($request->month == 5){
    		$monthName = 'May';
    	}elseif($request->month == 6){
    		$monthName = 'June';
    	}elseif($request->month == 7){
    		$monthName = 'July';
    	}elseif($request->month == 8){
    		$monthName = 'August';
    	}elseif($request->month == 9){
    		$monthName = 'September';
    	}elseif($request->month == 10){
    		$monthName = 'October';
    	}elseif($request->month == 11){
    		$monthName = 'November';
    	}elseif($request->month == 12){
    		$monthName = 'December';
    	}

    	$dt = Carbon::create($request->year, $request->month, 1, 0, 0, 0, 'Asia/Manila');
    	$dt2 = $carbon = new Carbon('last day of '.$monthName.' '.$request->year.'');

    	$eligible = invites::select(DB::raw('DISTINCT inviter'))
					->groupBy('inviter')
					->havingRaw('COUNT(inviter) >= 5')
					->where('created_at', '>', $dt)
					->where('created_at', '<', $dt2)
					->get();
        // dd($eligible);
        if ($eligible->count() == 0) {
            $mspValue = 0;
        }else{
            $mspValue = (float)$msp->eAmount * 5;
        }
        

    	$month = new msp;
    	$month->month = $request->month;
    	$month->by = $userid;
    	$month->year = $request->year;
    	$month->member_count = $eligible->count();
    	$month->distributed = 0;
        $month->amount = $mspValue;
    	$month->save();
        $mspsid = $month->id;
        $userid = Auth::user()->id;
        $username = Auth::user()->username;
        $description = $username.' created mps of '.$monthName.' '.$request->year;
        $this->activityLogs($description, $userid, $mspsid);
        foreach($eligible as $e){
            // $this->addEarnings($e->inviter, 10, $mspValue);
            $this->qualified($mspsid, $e->inviter);
        }
    	return redirect('msp');
    }

    private function addEarnings($userid, $earningType, $amount){
        $payout = userFunds::where('userid', $userid)->first();

        if($payout){
           $payout->payoutFunds += $amount;
            $payout->save(); 
        }else{
            $payout = new userFunds;
            $payout->userid = $userid;
            $payout->registrationFunds = 0;
            $payout->payoutFunds = $amount;
            $payout->save();
        }

        $earning = new earnings;
        $earning->userid = $userid;
        $earning->earningTypeId = $earningType;
        $earning->amount = $amount;
        $earning->save();
    }
    private function qualified($mpsid, $qualifiedid){
        $qualify = new msps_details;
        $qualify->msps_id = $mpsid;
        $qualify->qualified = $qualifiedid;
        $qualify->save();
    }
    private function activityLogs($description, $userid, $transactionid){
        $logs = new activity_logs;
        $logs->description = $description;
        $logs->by  = $userid;
        $logs->transaction_id = $transactionid;
        $logs->type = 2;
        $logs->save();
        return 'sucess';
    }
}
// type
// 1 encashment
// 2 mps
